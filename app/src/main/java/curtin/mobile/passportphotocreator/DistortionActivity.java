package curtin.mobile.passportphotocreator;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import static android.util.FloatMath.pow;
import static android.util.FloatMath.sqrt;
import static java.lang.Math.atan;

public class DistortionActivity extends ActionBarActivity {

    Bitmap lowResImage;
    Context context;
    String imagePath;

    LinearLayout loadingText;
    Button btnMinus;
    Button btnPlus;
    ImageView imageView;
    SeekBar distBar;
    TextView distortValue;
    Bitmap theImage;

    //This is to stop multiple uses of the distortion function which lags everything up.
    boolean isInUse = false;

    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            Bundle bundle = msg.getData();
            try
            {
                if(Float.toString((float)distBar.getProgress()/10).equals(distortValue.getText().toString()))
                {
                    imageView.setImageBitmap((Bitmap) bundle.getParcelable("theUpdatedBitmap"));
                    loadingText.setVisibility(View.GONE);
                    isInUse = false;
                }
                else
                {
                    updateSeek(distBar.getProgress());
                }
            }
            catch(Exception E)
            {
                System.out.println(E.getMessage());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distortion);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        imagePath = (extras.getString("IMAGE_PATH"));
        theImage = BitmapFactory.decodeFile(imagePath);

        imageView = (ImageView) findViewById(R.id.imagePreview);
        lowResImage = Bitmap.createScaledBitmap(theImage,
                theImage.getWidth() / 3, theImage.getHeight() / 3, true);
        imageView.setImageBitmap(lowResImage);

        loadingText = (LinearLayout)findViewById(R.id.loading_text);
        loadingText.setVisibility(View.GONE);

        distBar = (SeekBar) findViewById(R.id.distBar);
        distBar.setMax(30);
        distBar.setProgress(0);
        distortValue = (TextView) findViewById(R.id.distortValue);

        /*
         *Set up Plus and Minus Listeners
         *
         *Minus Button
         */
        btnMinus = (Button)findViewById(R.id.btn_minus);
        btnMinus.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if(!isInUse)
                {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            findViewById(R.id.btn_minus).setAlpha((float) 0.5);
                            break;

                        case MotionEvent.ACTION_UP:
                            findViewById(R.id.btn_minus).setAlpha((float) 1.0);
                            //Make a loading toast that wont disappear..
                            loadingText.setVisibility(View.VISIBLE);

                            distBar.setProgress(distBar.getProgress() - 1);
                            break;
                    }
                }
                return true;
            }
        });
        /*
        Plus Button
         */
        btnPlus = (Button)findViewById(R.id.btn_plus);
        btnPlus.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if(!isInUse)
                {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            findViewById(R.id.btn_plus).setAlpha((float) 0.5);
                            //Make a loading toast that wont disappear..
                            loadingText.setVisibility(View.VISIBLE);
                            break;

                        case MotionEvent.ACTION_UP :
                            findViewById(R.id.btn_plus).setAlpha((float) 0.5);
                            //Make a loading toast that wont disappear..
                            loadingText.setVisibility(View.VISIBLE);

                            distBar.setProgress(distBar.getProgress() + 1);
                            findViewById(R.id.btn_plus).setAlpha((float) 1.0);
                            break;
                    }
                }
                return true;
            }
        });



        //This is the listener used for scrolling the rotation seekbar. It changes the
        //rotation of the low res image when moved
        distBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser)
            {
                if(!isInUse)
                {
                    isInUse = true;
                    //Make a loading toast that wont disappear..
                    loadingText.setVisibility(View.VISIBLE);
                    distortValue.setText(String.valueOf((float) (progress) / 10));
                    Runnable runnable = new Runnable()
                    {
                        public void run()
                        {
                            Message msg = handler.obtainMessage();
                            Bundle bundle = new Bundle();
                            Bitmap bitmap = distortBitmap(lowResImage, (float) (progress) / 10, 1);
                            bundle.putParcelable("theUpdatedBitmap", (Parcelable)bitmap);
                            msg.setData(bundle);
                            handler.sendMessage(msg);
                        }
                    };

                    Thread myThread = new Thread(runnable);
                    myThread.start();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });


        //This is the listener for the button used to reset the rotation back to default
        Button btnReset = (Button)findViewById(R.id.resetButton);
        btnReset.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //Make a loading toast that wont disappear..
                        loadingText.setVisibility(View.VISIBLE);
                        findViewById(R.id.resetButton).setAlpha((float) 0.5);
                        break;

                    case MotionEvent.ACTION_UP:
                        findViewById(R.id.resetButton).setAlpha((float) 1.0);
                        distBar.setProgress(0);
                        loadingText.setVisibility(View.GONE);
                        break;
                }
                return true;
            }
        });

        context = this;
        //This is the listener for the button used to go to the next screen, presumably cropping
        Button btnNext = (Button)findViewById(R.id.nextButton);
        btnNext.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if(!isInUse)
                {
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            //Make a loading toast that wont disappear..
                            loadingText.setVisibility(View.VISIBLE);
                            findViewById(R.id.nextButton).setAlpha((float) 0.5);
                            break;

                        case MotionEvent.ACTION_UP:
                            findViewById(R.id.nextButton).setAlpha((float) 1.0);
                            //Start a new intent and move to next activity.
                            if (theImage != null)
                            {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inMutable = true;

                                Bitmap newImage = distortBitmap(theImage.copy(Bitmap.Config.ARGB_8888, true), (float) (distBar.getProgress()) / 10, 1);

                                Intent intent = new Intent(context, ImageEditingActivity.class);
                                intent.putExtra("IMAGE", createImageFromBitmap(newImage));
                                startActivity(intent);
                                finish();
                                }
                            break;
                    }
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rotation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    Anther way to change the seekbar stats and update images. only used in callback Handler.
     */
    private void updateSeek(final int progress)
    {
        isInUse = true;
        //Make a loading toast that wont disappear..
        loadingText.setVisibility(View.VISIBLE);
        distortValue.setText(String.valueOf((float) (progress) / 10));
        Runnable runnable = new Runnable()
        {
            public void run()
            {
                Message msg = handler.obtainMessage();
                Bundle bundle = new Bundle();
                Bitmap bitmap = distortBitmap(lowResImage, (float) (progress) / 10, 1);
                bundle.putParcelable("theUpdatedBitmap", (Parcelable)bitmap);
                msg.setData(bundle);
                handler.sendMessage(msg);
            }
        };

        Thread myThread = new Thread(runnable);
        myThread.start();
    }

    public Bitmap distortBitmap(Bitmap source, float strength, float zoom)
    {
        float halfWidth = source.getWidth()/2;
        float halfHeight = source.getHeight()/2;

        float correctionRadius = sqrt(pow(source.getWidth(), 2) + pow(source.getHeight(), 2))/strength;

        //New Bitmap - so that the source isnt distorted
        Bitmap newBit = Bitmap.createBitmap(source);

        //Algorithm taken from: http://www.tannerhelland.com/4743/simple-algorithm-correcting-lens-distortion/
        /*
        Loop though all the pixels!
         */
        for(int x = 0; x < newBit.getWidth(); x++)
        {
            for(int y = 0; y < newBit.getHeight(); y++)
            {
                float newX = x - halfWidth;
                float newY = y - halfHeight;

                float distance = sqrt(pow(newX, 2) + pow(newY, 2));
                float r = distance / correctionRadius;

                float theta;
                if(r == 0)
                {
                    theta = 1;
                }
                else
                {
                    theta = (float)atan(r)/r;
                }

                float sourceX = halfWidth + theta * newX * zoom;
                float sourceY = halfHeight + theta * newY * zoom;

                newBit.setPixel(x, y, source.getPixel((int)sourceX, (int) sourceY));
            }
        }
        return newBit;
    }


    public String createImageFromBitmap(Bitmap bitmap)
    {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }
}
