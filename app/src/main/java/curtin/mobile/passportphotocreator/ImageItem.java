package curtin.mobile.passportphotocreator;

import android.graphics.Bitmap;

/**
 * Created by neel on 26/03/2015.
 */

//This class's objects are used to store a set of image and its name.
public class ImageItem {
    private Bitmap image;
    private String title;

    public ImageItem(Bitmap image, String title) {
        super();
        this.image = image;
        this.title = title;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

