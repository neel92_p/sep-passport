package curtin.mobile.passportphotocreator;
/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceFragment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;


public class MainActivity extends ActionBarActivity {


    private static final int SELECT_PICTURE = 1;
    private static final int SELECT_PHOTO = 100;
    private String selectedImagePath; //String holds the image selected from the gallery picker

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor sharedPrefEditor;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Get shared preferences and open them for ediding.
        sharedPref = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();

        //CHECK TO SEE THIS IS THE FIRST TIME THE APP IS LOADED.
        //The variable isStartupScreenAlreadyDisplayed will set to false if this is not the first time.
        //If it is the first time, then the Startup_settings activity needs to be shown.

        Boolean wasPreviouslyDisplayed = sharedPref.getBoolean("isStartupScreenAlreadyDisplayed", false);
        if (!wasPreviouslyDisplayed)
        {
            Intent startInitialSetupScreen = new Intent(this, Startup_settings.class);
            startActivity(startInitialSetupScreen);

        }

        //Create the directory to store all the images that are taken.
        //This directory is used to store all the images used to store any pictures created.

        try {

            //Get the directory name from the android string resources.
            //This way of storing strings makes sure the string is accessible from throughout the application.
            String directoryLocation = getString(R.string.directory);

            // create a File object for the parent directory
            File imagesDirectory = new File(directoryLocation);
            // have the object build the directory structure, if needed.
            imagesDirectory.mkdirs();


            /***
             * The code commented out is used to test if the file is created in the right location.
             *
             */

            //File outputFile = new File(imagesDirectory, "test.png");
            //FileOutputStream fos = new FileOutputStream(outputFile);

        }
        catch (Exception e)
        {
            //Toast if an error occurs.
            Toast.makeText(MainActivity.this, "Unable to create directory/file in the Internal Storage.",
                    Toast.LENGTH_LONG).show();
        }

        /*
         *JUMP TO TIMS STUFF  vvvvvvvvvvvvvvvvvvvvvvvvvvvvv
         * /**/

        //Get Sample image from /drawable folder and convert it to a bitmap
//        Drawable myDrawable = getResources().getDrawable(R.drawable.snorkleguy);
//        Bitmap myImage = ((BitmapDrawable) myDrawable).getBitmap();
//
//        //Save image to an internal file
//        String imgFileName = createImageFromBitmap(myImage);
//
//        Intent intent = new Intent(this, CropActivity.class);
//        intent.putExtra("imgFileName", imgFileName);
//        startActivity(intent);
//         /**/
//
//        //Get width of screen in pixels (For formatting)
//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        screenWidth = metrics.widthPixels;
        /*
        TIMS STUFF  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         */




        //These are all the buttons whose listeners we'll create to have touch effects.
        final ImageButton cameraButton = (ImageButton) findViewById(R.id.imageView_cameraButton);
        final ImageButton galleryButton = (ImageButton) findViewById(R.id.openGallery_imageView);
        final ImageButton projectsButton = (ImageButton) findViewById(R.id.oldProjects_imageButton);
        final TextView cameraTextView = (TextView) findViewById(R.id.camera_textView);
        final TextView galleryTextView = (TextView) findViewById(R.id.gallery_textView);
        final TextView projectsTextView = (TextView) findViewById(R.id.projectsTextView);


        //Touch listener on the galleryTextView kind of extends the ability of the camera button.
        //It does the same thing the button does but using this gives the user a larger area to touch on.
        cameraTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //ImageView is the background whose color is changed.
                //ImageView cameraView = (ImageView) findViewById(R.id.ImageView_CameraView);
                LinearLayout cameraView = (LinearLayout) findViewById(R.id.ImageView_CameraView);
                switch (event.getAction())
                {
                    //When the button is pressed, the color is changed.
                    case MotionEvent.ACTION_DOWN:
                    {
                        cameraView.setBackgroundColor(Color.argb(50, 0 , 0, 255));
                        /**
                         * Disable others buttons so that not all of them can be clicked together.
                         */
                        projectsButton.setEnabled(false);
                        galleryButton.setEnabled(false);
                        break;
                    }
                    //When the button is released, the color is changed back to the normal color.
                    case MotionEvent.ACTION_UP:
                    {
                        cameraView.setBackgroundColor(Color.WHITE);
                        /**
                         * Enable the buttons again.
                         */
                        projectsButton.setEnabled(true);
                        galleryButton.setEnabled(true);

                        //Start the camera activity. This function call does exactly this.
                        startCameraActivity();


                        break;
                    }
                }

                return true;
            }
        });


        /**
         * These listeners are  used to create  touch effects by changing color of the background of the
         * buttons selected.
         */

        //Listener for the touch on the button that is used to create a camera.
        cameraButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                //ImageView is the background whose color is changed.
                //ImageView cameraView = (ImageView) findViewById(R.id.ImageView_CameraView);
                LinearLayout cameraView = (LinearLayout) findViewById(R.id.ImageView_CameraView);
                switch (event.getAction())
                {
                    //When the button is pressed, the color is changed.
                    case MotionEvent.ACTION_DOWN:
                    {
                        cameraView.setBackgroundColor(Color.argb(50, 0 , 0, 255));
                        /**
                         * Disable others buttons so that not all of them can be clicked together.
                         */
                        projectsButton.setEnabled(false);
                        galleryButton.setEnabled(false);
                        break;
                    }
                    //When the button is released, the color is changed back to the normal color.
                    case MotionEvent.ACTION_UP:
                    {
                        cameraView.setBackgroundColor(Color.WHITE);
                        /**
                         * Enable the buttons again.
                         */
                        projectsButton.setEnabled(true);
                        galleryButton.setEnabled(true);

                        //Start the camera activity. This function call does exactly this.
                        startCameraActivity();


                        break;
                    }
                }

                return true;
            }
        });


        //Touch listener on the galleryTextView kind of extends the ability of the gallery button.
        //It does the same thing the button does but using this gives the user a larger area to touch on.
        galleryTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //ImageView galleryView = (ImageView) findViewById(R.id.galleryView);
                LinearLayout galleryView = (LinearLayout) findViewById(R.id.galleryView);
                switch (event.getAction())
                {
                    //When the button is pressed, the color is changed.
                    case MotionEvent.ACTION_DOWN:
                    {
                        galleryView.setBackgroundColor(Color.argb(50, 0, 0, 255));

                        /**
                         * When the button is pressed, the other buttons cannot be pressed with it.
                         * So we set the other buttons to disabled so that they can't be selected.
                         */
                        cameraButton.setEnabled(false);
                        projectsButton.setEnabled(false);


                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        galleryView.setBackgroundColor(Color.WHITE);
                        /**
                         * Enable the buttons again.
                         */
                        cameraButton.setEnabled(true);
                        projectsButton.setEnabled(true);

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                        break;
                    }
                }
                return true;
            }
        });

        /**
         * This is the touch listener for the button that is used to access the gallery.
         */
        galleryButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //ImageView galleryView = (ImageView) findViewById(R.id.galleryView);
                LinearLayout galleryView = (LinearLayout) findViewById(R.id.galleryView);
                switch (event.getAction())
                {
                    //When the button is pressed, the color is changed.
                    case MotionEvent.ACTION_DOWN:
                    {
                        galleryView.setBackgroundColor(Color.argb(50, 0, 0, 255));

                        /**
                         * When the button is pressed, the other buttons cannot be pressed with it.
                         * So we set the other buttons to disabled so that they can't be selected.
                         */
                        cameraButton.setEnabled(false);
                        projectsButton.setEnabled(false);


                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        galleryView.setBackgroundColor(Color.WHITE);
                        /**
                         * Enable the buttons again.
                         */
                        cameraButton.setEnabled(true);
                        projectsButton.setEnabled(true);

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                        break;
                    }
                }
                return true;
            }
        });


        //Touch listener on the galleryTextView kind of extends the ability of the projects button.
        //It does the same thing the button does but using this gives the user a larger area to touch on.
        projectsTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //ImageView galleryView = (ImageView) findViewById(R.id.galleryView);
                LinearLayout galleryView = (LinearLayout) findViewById(R.id.projectsView);
                switch (event.getAction())
                {
                    //When the button is pressed, the color is changed.
                    case MotionEvent.ACTION_DOWN:
                    {
                        galleryView.setBackgroundColor(Color.argb(50, 0, 0, 255));

                        /**
                         * When the button is pressed, the other buttons cannot be pressed with it.
                         * So we set the other buttons to disabled so that they can't be selected.
                         */
                        cameraButton.setEnabled(false);
                        projectsButton.setEnabled(false);


                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        galleryView.setBackgroundColor(Color.WHITE);
                        /**
                         * Enable the buttons again.
                         */
                        cameraButton.setEnabled(true);
                        projectsButton.setEnabled(true);

                        //Start the projects activity.
                        startProjectsActivity();
                        break;
                    }
                }
                return true;
            }
        });

        /**
         * This is the touch listener of the button that is clicked to get to older projects.
         */
        projectsButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //ImageView projectsView = (ImageView) findViewById(R.id.projectsView);
                LinearLayout projectsView = (LinearLayout) findViewById(R.id.projectsView);
                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                    {
                        //When the button is pressed, the color is changed.
                        projectsView.setBackgroundColor(Color.argb(50, 0, 0, 255));
                        /**
                         * When the button is pressed, the other buttons cannot be pressed with it.
                         * So we set the other buttons to disabled so that they can't be selected.
                         */
                        cameraButton.setEnabled(false);
                        galleryButton.setEnabled(false);
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    {
                        projectsView.setBackgroundColor(Color.WHITE);
                        /**
                         * Enable the buttons again.
                         */
                        cameraButton.setEnabled(true);
                        galleryButton.setEnabled(true);


                        //Start Projects activity
                        startProjectsActivity();
                        break;
                    }

                }
                return true;
            }
        });


       /*
        galleryButton
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);

                    }
                });

                */


    }



    //This is the listener for the camera_button. It's used to start the activity CameraPreview.
    public void startCameraActivity()
    {
        //Intent to start the activity for Camera_Screen.
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    //This function will start the Projects activity.
    public void startProjectsActivity()
    {
        Intent intent = new Intent(this, Projects.class);
        startActivity(intent);
    }


    public void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            if (requestCode == SELECT_PHOTO || requestCode == SELECT_PICTURE)
            {
                Uri selectedImageUri = data.getData();
                //selectedImagePath = getPath(selectedImageUri);
                selectedImagePath = getPath(this, selectedImageUri);
                String path = selectedImageUri.getPath();

                Boolean imageSupported = true; //Set this when the image is not supported.
                //This is to make sure that the only images that can be picked from the gallery
                //are JPEGs and PNGs. Any other ones will be rejected.
                if (!(selectedImagePath.endsWith(".png") || selectedImagePath.endsWith(".jpg") || selectedImagePath.endsWith(".jpeg") ||
                        selectedImagePath.endsWith(".PNG") || selectedImagePath.endsWith(".JPG") || selectedImagePath.endsWith(".JPEG")))
                {
                    imageSupported = false;
                    Toast.makeText(this, "Unsupported Image. Ensure it is a PNG, JPG, or JPEG.", Toast.LENGTH_LONG)
                            .show();
                }

                if (selectedImagePath != null && imageSupported)

                {
                    startPreviewActivity();
                }
                else
                {
                    //                    new AlertDialog.Builder(this)
//                            .setTitle("Unsupported Image")
//                            .setMessage("Image unsupported or not found.")
//                            .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    //do nothing.
//                                    dialog.cancel();
//                                }
//                            })
//                            .show();

                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
        }

        if (id==R.id.help)
        {
            //This code is used to load the user pdf when the button is clicked.
            openUserGuide();
        }

        return super.onOptionsItemSelected(item);
    }

    //This function is used to copy the user guide from the assets folder in the application
    //to a public location on the phone. Then a pdf reader can open the user guide.
    private void openUserGuide()
    {
        File fileBrochure = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "User Guide - Passport Photo Creator.pdf");
        if (!fileBrochure.exists())
        {
            CopyAssetsbrochure();
        }

        /** PDF reader code */
        File file = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "User Guide - Passport Photo Creator.pdf");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file),"application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        //If no PDF reader is found then a message is shown to  indicate that no PDF reader is found.
        try
        {
            getApplicationContext().startActivity(intent);
        }
        catch (ActivityNotFoundException e)
        {
            Toast.makeText(MainActivity.this, "No PDF reader found. Please install one before trying again.", Toast.LENGTH_SHORT).show();
        }
    }

    //method to write the PDFs file to sd card
    private void CopyAssetsbrochure() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try
        {
            files = assetManager.list("");
        }
        catch (IOException e)
        {
            Log.e("tag", e.getMessage());
        }
        for(int i=0; i<files.length; i++)
        {
            String fStr = files[i];
            //The string passed to the equalsIgnoreCase is used the file name of the user guide.
            if(fStr.equalsIgnoreCase("User Guide - Passport Photo Creator.pdf"))
            {
                InputStream in = null;
                OutputStream out = null;
                try
                {
                    in = assetManager.open(files[i]);
                    out = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + File.separator + files[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                    break;
                }
                catch(Exception e)
                {
                    Log.e("tag", e.getMessage());
                }
            }
        }
    }

    //This function is used to copy a file from the input to output stream.
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }




    /**
     * This function is called to create a preview of the image and put it in the
     * preview box.
     * @param path - path of the image selected.
     */
//    public void retrieveImage (String path)
//    {
//
//
//        ImageView imageView = (ImageView) findViewById(R.id.imageView);
//
//        if (path != null) {
//            //ImageManipulation class has tools to reduce resolution and rotate images, etc.
//            //The 300 is the width required. The 400 is the height of the image after the
//            //resolution has been reduced.
//            Bitmap lowerResolutionImage = ImageManipulation.lessResolution(path, 480, 640);
//            lowerResolutionImage = ImageManipulation.rotateImage(lowerResolutionImage, 90);
//            //Use the lowerResolutionImage is displayed on the imageView of this activity.
//            imageView.setImageBitmap(lowerResolutionImage);
//        }
//
//    }

    /**
     * This function starts the "Preview" activity when the function is called.
     */
    public void startPreviewActivity ()
    {
        //Param imagePath is the path to the image selected by the user,
        //or the path of the photo recently taken.
        //Create an intent to start the application.
        Intent intent = new Intent (this, Preview.class);

        //Set extra information via put extra. Send the path to the image (selectedImagePath).
        if (selectedImagePath != null)
        {
            intent.putExtra("IMAGE_PATH", selectedImagePath);
            startActivity(intent);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


//    public String getPath (Uri uri)
//    {
//
//        String [] projection = {MediaStore.Images.Media.DATA};
//        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
//        if (cursor != null)
//        {
//            cursor.moveToFirst();
//            int columnIndex = cursor.getColumnIndex(projection[0]);
//            String picturePath = cursor.getString(columnIndex);
//            cursor.close();
//            if(picturePath != null)
//                return picturePath;
//            else
//                System.out.println("FilePath is null");
//
//        }
//
//        //This is our fallback here.
//        return uri.getPath();
//    }

    /*
    Saves a bitmap to a file inside the app.

    */

//    public String createImageFromBitmap(Bitmap bitmap)
//    {
//        String fileName = "myImage";
//        try
//        {
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
//            fo.write(bytes.toByteArray());
//            fo.close();
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//            fileName = null;
//        }
//        return fileName;
//    }


    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }
}
