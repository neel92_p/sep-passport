package curtin.mobile.passportphotocreator;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileNotFoundException;

/**
 * Created by Lami on 14/05/2015.
 */
public class GlobalImageData extends Application {
    private Bitmap imageStageOne;
    private Bitmap imageStageTwo;
    private Bitmap imageStageThree;
    private Bitmap imageStageFour;
    private Bitmap cropImage;

    public GlobalImageData()
    {
        imageStageOne = null;
        imageStageTwo = null;
        imageStageThree = null;
        imageStageFour = null;
        cropImage = null;
    }

    public void setImage(String image)
    {
        try
        {
            imageStageOne = BitmapFactory.decodeStream(openFileInput(image));
        }
        catch(FileNotFoundException E)
        {
            System.out.println("Image not found.");
        }
    }

    public void updateImageTwo(Bitmap inImage)
    {
        imageStageTwo = inImage;
    }

    public void updateImageThree(Bitmap inImage)
    {
        imageStageThree = inImage;
    }

    public void updateImageFour(Bitmap inImage) { imageStageFour = inImage; }

    public void updateCropImage(Bitmap inImage)
    {
        cropImage = inImage;
    }

    public Bitmap getImage(int i)
    {
        if(i == 1)
            return imageStageOne;
        else if(i == 2)
            return imageStageTwo;
        else if(i == 3)
            return imageStageThree;
        else if(i == 4)
            return imageStageFour;
        else if(i == 5)
            return cropImage;
        else
            return null;
    }
}
