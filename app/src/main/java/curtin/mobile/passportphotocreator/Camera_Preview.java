package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;


public class Camera_Preview extends ActionBarActivity {

    Button retakeButton;
    Button yesButton;
    ImageView imageView;
    String imageName;
    String absolutePathToPreviousPicture;
    FrameLayout layout;
    Context context;

    private Menu menu;

    //OnClickListener for the retakeButton.
    //It starts the camera activity again if the image does not like it.
    OnClickListener retakeButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            startCameraActivity(); //Start camera activity with function call.
        }
    };

    /*
    Show a pop up asking if user wants to correct distortion to image.
    If user selects yes, go on to Distortion activity.
    If user selects no, go on to image editing activity.
     */
    OnClickListener yesButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

        //Create the pop up
        LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.to_distortion_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                AbsoluteLayout.LayoutParams.WRAP_CONTENT,
                AbsoluteLayout.LayoutParams.WRAP_CONTENT);

        //Dim background
        layout.getForeground().setAlpha(180);

        final Button btnYes = (Button)popupView.findViewById(R.id.yes_button);
        final Button btnNo = (Button)popupView.findViewById(R.id.no_button);

            btnYes.setOnTouchListener(new Button.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent theMotion) {
                    switch (theMotion.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            btnYes.setAlpha((float) 0.5);
                            break;
                        case MotionEvent.ACTION_UP:
                            popupWindow.dismiss();
                            btnYes.setAlpha((float) 1.0);
                            layout.getForeground().setAlpha(0);

                            Intent intent = new Intent(context, DistortionActivity.class);
                            intent.putExtra("IMAGE_PATH", absolutePathToPreviousPicture);
                            System.out.println("Test line before intent");
                            startActivity(intent);
                            finish();
                            break;
                    }
                    return true;
                }
            });

            //What happens if Next button is pressed.
            btnNo.setOnTouchListener(new Button.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent theMotion) {
                    switch (theMotion.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            btnNo.setAlpha((float) 0.5);
                            break;
                        case MotionEvent.ACTION_UP:
                            btnNo.setAlpha((float) 1.0);
                            popupWindow.dismiss();
                            layout.getForeground().setAlpha(0);

                            String imagePath = createImageFromBitmap(BitmapFactory.decodeFile(absolutePathToPreviousPicture));

                            Intent intent = new Intent(context, ImageEditingActivity.class);
                            intent.putExtra("IMAGE", imagePath);
                            System.out.println("Test line before intent");
                            startActivity(intent);
                            finish();
                            break;
                    }
                    return true;
                }
            });

        //Show the Window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, -1, -1);
    }

    };

    //Function to start camera activity
    public void startCameraActivity()
    {
        //Create the activity
        Intent intent = new Intent(this, CameraActivity.class);

        //Delete the previously taken image.
        File previousPicture = new File(absolutePathToPreviousPicture);
        if (previousPicture.exists())
        {
            previousPicture.delete();
        }
        //Stop this activity and start the camera.
        finish();
        startActivity(intent);


    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_blue_red);

        //No action bar is needed in this activity. The preview activity needs to be full-screen. This hides that action bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        context = this;

        //Set up for dimming with popup.
        layout = (FrameLayout) findViewById( R.id.mainmenu);
        //Dim level is set at 0. when a popup happens, the background will be dimmed. looks cool as hell.
        layout.getForeground().setAlpha(0);

        ///Initialize buttons and set listeners.
        //The retake buttons deletes the previously captured image and then starts the camera activity for another picture.
        retakeButton = (Button) findViewById(R.id.retakeButton);
        retakeButton.setOnClickListener(retakeButtonOnClickListener);

        //The yes buttons starts the image editing activity.
        yesButton = (Button) findViewById(R.id.yesButton);
        yesButton.setOnClickListener(yesButtonOnClickListener);

        imageView = (ImageView) findViewById(R.id.imageView);


        //Get the filename (name of the last picture taken) passed in by the main_activity.
        //This was sent by addExtra with the name LAST_PICTURE_FILENAME.

        if (savedInstanceState == null)
        {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                imageName = null;
            }
            else
            {
                imageName = extras.getString("LAST_PICTURE_FILENAME");
            }
        }
        else
        {
            imageName = (String) savedInstanceState.getSerializable("LAST_PICTURE_FILENAME");
        }

        //If I retake the image, the currently displayed photo will be deleted.
        String directoryLocation = getString(R.string.directory);
        absolutePathToPreviousPicture = Environment.getExternalStorageDirectory().getPath() + File.separator + directoryLocation + File.separator + imageName;
        //Call function displayImage to set the image in the view.
        displayImage(imageName);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_camera__preview, menu);

        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //The settings button starts the preferences activity.
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }

    /**
     * This function is used to display the image captured, to decide what to do next.
     * @param imageName : the name of the picture that the last capture was saved as.
     */
    private void displayImage (String imageName)
    {
        //Once the filename of the image is found, read the file and display the picture as a bitmap in the imageView ImageView.
        String directoryLocation = getString(R.string.directory); //Get the directory in which all the images are stored.
        String absolutePath = Environment.getExternalStorageDirectory().getPath() + File.separator + directoryLocation + File.separator + imageName;

        if (absolutePath != null)
        {
            //ImageManipulation object is used to rotate the object.
            ImageManipulation imgManipulation = new ImageManipulation();
            BitmapFactory.Options options = new BitmapFactory.Options();

            try {
                //Decode the file to a bitmap that can be displayed.
                Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, options);
                //Rotate the image so that it is displayed as portrait
                bitmap = imgManipulation.rotateImage(bitmap, 90);
                //The image is displayed on the imageView (The ImageView to display the image, see XML file).
                imageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public String createImageFromBitmap(Bitmap bitmap)
    {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }

}
