package curtin.mobile.passportphotocreator;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrintReadyActivity extends ActionBarActivity {
    private ImageView imageView;
    private Spinner picSpinner;
    private Switch borderSwitch;
    private Switch distortSwitch;
    private Spinner mediaSpinner;
    private int mediaWidth, mediaHeight;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_ready);
        imageView = (ImageView) findViewById(R.id.imagePreview);
        picSpinner = (Spinner) findViewById(R.id.spinner1);
        mediaSpinner = (Spinner) findViewById(R.id.mediaSpinner);
        borderSwitch = (Switch) findViewById(R.id.toggleBorders);
        distortSwitch = (Switch) findViewById(R.id.toggleDistortion);
        mediaWidth = 1800;
        mediaHeight = 1200;
        checkPicDimensions();

        imageView.setImageBitmap(drawImage(1, mediaWidth/3, mediaHeight/3, false, false));

        picSpinner.setSelection(0);
        picSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String pos = picSpinner.getItemAtPosition(position).toString();
                int i = Integer.parseInt(pos);
                imageView.setImageBitmap(drawImage(i, mediaWidth/3, mediaHeight/3, borderSwitch.isChecked(), distortSwitch.isChecked()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mediaSpinner.setSelection(0);
        mediaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0)
                {
                    mediaHeight = 1200;
                    mediaWidth = 1800;
                }
                else if (position == 1)
                {
                    mediaHeight = 1500;
                    mediaWidth = 2100;
                }
                int value = (picSpinner.getSelectedItemPosition()+1);
                imageView.setImageBitmap(drawImage(value, mediaWidth/3, mediaHeight/3, borderSwitch.isChecked(), distortSwitch.isChecked()));
                checkPicDimensions();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        borderSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                int value = (picSpinner.getSelectedItemPosition()+1);
                imageView.setImageBitmap(drawImage(value, mediaWidth/3, mediaHeight/3, isChecked, distortSwitch.isChecked()));
            }
        });

        distortSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                int value = (picSpinner.getSelectedItemPosition()+1);
                imageView.setImageBitmap(drawImage(value, mediaWidth/3, mediaHeight/3, borderSwitch.isChecked(), isChecked));
            }
        });

        final Button saveButton = (Button)findViewById(R.id.saveButton);
        saveButton.setOnTouchListener(new Button.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion)
            {
                switch (theMotion.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        saveButton.setAlpha((float)0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        //Create Path to save Image
                        int value = (picSpinner.getSelectedItemPosition()+1);
                        SimpleDateFormat date = new SimpleDateFormat("HHmmss");
                        String imgName = "PPImage-" + date.format(new Date());
                        Bitmap bm = drawImage(value, mediaWidth/3, mediaHeight/3, borderSwitch.isChecked(), distortSwitch.isChecked());
                        String directory = getString(R.string.directory);
                        File path = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + directory); //Creates app specific folder
                        path.mkdirs();
                        File imageFile = new File(path, imgName+".png"); // Imagename.png
                        try {
                            FileOutputStream out = new FileOutputStream(imageFile);
                            bm.compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
                            out.flush();
                            out.close();


                            Context context = getApplicationContext();
                            CharSequence text = "Image Saved";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            saveButton.setAlpha((float)1.0);
                        }
                        catch(Exception e)
                        {
                            System.out.println(e.getMessage());
                        }
                        break;
                }
                return true;
            }
        });
    }

    public void checkPicDimensions()
    {
        SharedPreferences settings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        final String country = settings.getString("settings_country", "default");
        final PhotoDimensionsForCountry photoDims = new PhotoDimensionsForCountry();
        if(((((photoDims.getHeight(country)*0.03937008*300)/mediaHeight)*mediaHeight)*2) > (mediaHeight -100))
        {
            String options[] = {"1", "2", "3"};
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, options);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            picSpinner.setAdapter(spinnerAdapter);
        }
        else
        {
            String options[] = {"1", "2", "3", "4", "5", "6"};
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, options);
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            picSpinner.setAdapter(spinnerAdapter);
        }
    }

    //Dirtiest code I have written since machine perceptions
    public Bitmap drawImage(int numPictures, int width, int height, boolean borders, boolean distort)
    {
        SharedPreferences settings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        final String country = settings.getString("settings_country", "default");
        final PhotoDimensionsForCountry photoDims = new PhotoDimensionsForCountry();
        GlobalImageData data = ((GlobalImageData)getApplicationContext());
        Paint paint = new Paint();
        Bitmap distortImage;
        paint.setColor(Color.BLACK);

        int paddingWidth, paddingHeight, picHeight, picWidth, tempPadWidth, tempPadHeight;
        if(numPictures < 4)
        {
            picHeight = 1;
            picWidth = numPictures;
        }
        else
        {
            picHeight = 2;
            picWidth = (int)Math.ceil((double)numPictures / 2.0);
        }
        double scaledHeight, scaledWidth;
        scaledHeight = ((photoDims.getHeight(country)*0.03937008*300)/mediaHeight)*height;
        scaledWidth =((photoDims.getWidth(country)*0.03937008*300)/mediaWidth)*width;
        Bitmap smallBitmap = Bitmap.createScaledBitmap(data.getImage(4), (int)(scaledWidth),
                (int)(scaledHeight), true);
        tempPadWidth = paddingWidth = (width - (picWidth * smallBitmap.getWidth()))/(picWidth+1);
        tempPadHeight = paddingHeight = (height - (picHeight * smallBitmap.getHeight()))/(picHeight+1);
        Bitmap bigBitmap = Bitmap.createBitmap(width, height , Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bigBitmap);
        canvas.drawRect(1, 1, width, height, paint);
        paint.setColor(Color.WHITE);
        canvas.drawRect(2, 2, width-1, height-1, paint);
        paint.setColor(Color.BLACK);
        int k = 0;
        for(int j = 0; j < picHeight; j++) {
            for(int i = 0; i < picWidth; i++) {
                if(k < numPictures) {

                    if(distort) {
                        if ((k == 1) || (k == 4)) {
                            distortImage = Bitmap.createScaledBitmap(data.getImage(4), (int) (scaledWidth) - 5,
                                    (int) (scaledHeight) - 5, true);
                        } else if ((k == 2) || (k == 5)) {
                            distortImage = Bitmap.createScaledBitmap(data.getImage(4), (int) (scaledWidth) + 5,
                                    (int) (scaledHeight) + 5, true);
                        } else {
                            distortImage = smallBitmap;
                        }
                        if (borders) {
                            canvas.drawRect(
                                    tempPadWidth - 3,
                                    tempPadHeight - 3,
                                    tempPadWidth + distortImage.getWidth() + 3,
                                    tempPadHeight + distortImage.getHeight() + 3,
                                    paint);

                        }
                        canvas.drawBitmap(distortImage, tempPadWidth, tempPadHeight, new Paint());
                        tempPadWidth += paddingWidth + distortImage.getWidth();
                        if((k == 2) && (numPictures != 4)) {
                            tempPadWidth = paddingWidth;
                            tempPadHeight += paddingHeight + smallBitmap.getHeight();
                        }
                        if(numPictures == 4)
                        {
                            if(k==1)
                            {
                                tempPadWidth = paddingWidth;
                                tempPadHeight += paddingHeight + smallBitmap.getHeight();
                            }
                        }
                        k++;
                    }
                    else {
                        if (borders) {
                            canvas.drawRect(
                                    (paddingWidth * (i + 1)) + (i * smallBitmap.getWidth()) - 3,
                                    (paddingHeight * (j + 1)) + (j * smallBitmap.getHeight()) - 3,
                                    (paddingWidth * (i + 1)) + ((i + 1) * smallBitmap.getWidth()) + 3,
                                    (paddingHeight * (j + 1)) + ((j + 1) * smallBitmap.getHeight()) + 3,
                                    paint);
                        }
                        canvas.drawBitmap(smallBitmap, (paddingWidth * (i + 1)) + (i * smallBitmap.getWidth()),
                                (paddingHeight * (j + 1)) + (j * smallBitmap.getHeight()), new Paint());
                        k++;
                    }
                }
            }
        }
        return bigBitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_print_ready, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }
}