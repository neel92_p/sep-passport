package curtin.mobile.passportphotocreator;

/**
 * Class CustomDrawView
 * This class extends DrawView (See below)
 * Author: Timothy Smith
 *
 * Student - Curtin University (13954048)
 * 2015
 *
 * ***********
 *
 * The class DrawView was found on StackOverflow
 * URL: http://stackoverflow.com/questions/10188478/how-to-make-view-resizable-on-touch-event
 *
 * Author: Chintan Rathod
 *
 * Accessed on 26/3/2015
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.util.ArrayList;

public class CustomDrawView extends DrawView {

    Point point1, point3;
    Point point2, point4;
    Point movePoint;

    public static final int THE_NUMBER = 120;

    /**
     * point1 and point 3 are of same group and same as point 2 and point4
     */
    int groupId = -1;
    private ArrayList<ColorBall> colorballs = new ArrayList<ColorBall>();
    // array that holds the balls
    private int balID = 0;
    // variable to know what ball is being dragged
    Paint paint;
    Canvas canvas;

    //Store height and width of View
    int minWidth = 0;
    int maxWidth = 0;
    int minHeight = 0;
    int maxHeight = 0;

    //Ratio
    double myRatio;
    //Bitmap imgBit;

    //Test ball
    int ballRadius;

    int viewWidth;
    int viewHeight;
    int picWidth;
    int picHeight;

    //When touch event occurs this is the position of the original touchdown.
    private int firstX;
    private int firstY;
    private int firstMoveX;
    private int firstMoveY;


    //Guidelines
    Bitmap guidelines;

    public CustomDrawView(Context context, double xPart, double yPart, int inWidth, int inHeight, int inViewWidth, int inViewHeight, Bitmap inGuidelines)
    {
        super(context);

        viewWidth = inViewWidth;
        viewHeight = inViewHeight;
        picWidth = inWidth;
        picHeight = inHeight;

        minWidth = (viewWidth - inWidth) / 2;
        maxWidth = inWidth + (viewWidth - inWidth) / 2;
        minHeight = (viewHeight - inHeight) / 2;
        maxHeight = inHeight + (viewHeight - inHeight) / 2;

        myRatio = xPart/yPart;

        paint = new Paint();
        setFocusable(true); // necessary for getting the touch events
        canvas = new Canvas();
        // setting the start point for the balls
        point1 = new Point();
        point1.x = viewWidth/2 - (int)(THE_NUMBER*myRatio)/2;
        point1.y = viewHeight/2 - THE_NUMBER/2;

        point2 = new Point();
        point2.x = viewWidth/2 + (int)(THE_NUMBER*myRatio)/2;
        point2.y = viewHeight/2 - THE_NUMBER/2;

        point3 = new Point();
        point3.x = viewWidth/2 + (int)(THE_NUMBER*myRatio)/2;
        point3.y = viewHeight/2 + THE_NUMBER/2;

        point4 = new Point();
        point4.x = viewWidth/2 - (int)(THE_NUMBER*myRatio)/2;
        point4.y = viewHeight/2 + THE_NUMBER/2;

        movePoint = new Point();
        movePoint.x = viewWidth/2;
        movePoint.y = viewHeight/2;


        // declare each ball with the ColorBall class
        colorballs.add(new ColorBall(context, R.drawable.drag_top_left, point1, 0));
        colorballs.add(new ColorBall(context, R.drawable.drag_bottom_left, point4, 1));
        colorballs.add(new ColorBall(context, R.drawable.drag_bottom_right, point3, 2));
        colorballs.add(new ColorBall(context, R.drawable.drag_top_right, point2, 3));
        colorballs.add(new ColorBall(context, R.drawable.drag_circle, movePoint, 4));

        //get guidelines
        guidelines = inGuidelines;
    }

    public CustomDrawView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /*public CustomDrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        setFocusable(true); // necessary for getting the touch events
        canvas = new Canvas();
        // setting the start point for the balls
        point1 = new Point();
        point1.x = 50;
        point1.y = 20;

        point2 = new Point();
        point2.x = 150;
        point2.y = 20;

        point3 = new Point();
        point3.x = 150;
        point3.y = 120;

        point4 = new Point();
        point4.x = 50;
        point4.y = 120;

        // declare each ball with the ColorBall class
        colorballs.add(new ColorBall(context, R.drawable.drag_circle, point1));
        colorballs.add(new ColorBall(context, R.drawable.drag_circle, point2));
        colorballs.add(new ColorBall(context, R.drawable.drag_circle, point3));
        colorballs.add(new ColorBall(context, R.drawable.drag_circle, point4));

    }*/

    // the method that draws the balls
    @Override
    protected void onDraw(Canvas canvas)
    {
        // canvas.drawColor(0xFFCCCCCC); //if you want another background color
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(Color.parseColor("#4D000000"));
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeJoin(Paint.Join.ROUND);
        // mPaint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(5);

        //This draws shaded area around the rectangle formed by the balls.
        canvas.drawRect(minWidth, minHeight, maxWidth, point1.y + colorballs.get(0).getWidthOfBall()/2, paint);
        canvas.drawRect(minWidth, point3.y + colorballs.get(0).getWidthOfBall()/2, maxWidth, maxHeight, paint);
        canvas.drawRect(minWidth, point1.y + colorballs.get(0).getWidthOfBall()/2, point1.x + colorballs.get(0).getWidthOfBall()/2, point3.y + colorballs.get(2).getWidthOfBall()/2, paint);
        canvas.drawRect(point3.x + colorballs.get(2).getWidthOfBall()/2, point1.y + colorballs.get(0).getWidthOfBall()/2, maxWidth, point3.y + colorballs.get(2).getWidthOfBall()/2, paint);
    

        BitmapDrawable mBitmap;
        mBitmap = new BitmapDrawable();

        // draw the balls on the canvas
        for (ColorBall ball : colorballs) {
            canvas.drawBitmap(ball.getBitmap(), ball.getX(), ball.getY(),
                    new Paint());

        }

        //draw the guidelines
        //get current width of cropbox
        int currWidth = colorballs.get(3).getX() - colorballs.get(0).getX();
        //get current Height of cropbox
        int currHeight = colorballs.get(1).getY() - colorballs.get(0).getY();
        //scale the bitmap to current wdith and height
        Bitmap scaledGuidelines = Bitmap.createScaledBitmap(guidelines, currWidth, currHeight, false);
        //paint guidelines onto canvas
        canvas.drawBitmap(scaledGuidelines, colorballs.get(0).getX() + colorballs.get(0).getWidthOfBall()/2, colorballs.get(0).getY() + colorballs.get(0).getWidthOfBall()/2, new Paint());

    }

    // events when touching the screen
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int eventAction = event.getAction();

        int X = (int) event.getX();
        int Y = (int) event.getY();

        switch (eventAction) {

            case MotionEvent.ACTION_DOWN: // touch down so check if the finger is on
                // a ball
                balID = -1;
                groupId = -1;
                firstX = X;
                firstY = Y;
                firstMoveX = colorballs.get(4).getX();
                firstMoveY = colorballs.get(4).getY();
                //boolean isBall = false;
                for (ColorBall ball : colorballs) {
                    // check if inside the bounds of the ball (circle)
                    // get the center for the ball
                    int centerX = ball.getX() + ball.getWidthOfBall();
                    int centerY = ball.getY() + ball.getHeightOfBall();
                    paint.setColor(Color.CYAN);
                    // calculate the radius from the touch to the center of the ball
                    double radCircle = Math
                            .sqrt((double) (((centerX - X) * (centerX - X)) + (centerY - Y)
                                    * (centerY - Y)));


                    if (radCircle < ball.getWidthOfBall())
                    {
                        //isBall = true;
                        balID = ball.getID();
                        System.out.println(balID);
                        if (balID == 1 || balID == 3)
                        {
                            groupId = 2;
                            canvas.drawRect(point1.x, point3.y, point3.x, point1.y,
                                    paint);
                        }
                        else if(balID == 0 || balID == 2)
                        {
                            groupId = 1;
                            canvas.drawRect(point2.x, point4.y, point4.x, point2.y,
                                    paint);
                        }
                        else
                        {
                            groupId = 3;
                            canvas.drawRect(point2.x, point4.y, point4.x, point2.y,
                                    paint);
                        }
                        invalidate();
                        break;
                    }
                    invalidate();
                }
                /**
                //no balls touched
                if(!isBall)
                {
                    balID = 5;
                    groupId = 4;
                }/**/

                break;

            case MotionEvent.ACTION_MOVE: // touch drag with the ball
                // move the balls the same as the finger
                if (balID > -1 && balID < 4)
                {
                    paint.setColor(Color.CYAN);

                    //radius of the colorball
                    ballRadius = colorballs.get(balID).getHeightOfBall()/2;

                    if (groupId == 1)//balls 0 or 2
                    {
                        //otherID refers to the id of the ball on the opposite corner of the cropbox
                        int otherID;
                        if(balID == 0)
                        {
                            otherID = 2;
                        }
                        else
                        {
                            otherID = 0;
                        }

                        //These two variables are for readability's sake. The new coordinates of the colorball.
                        int newX, newY;
                        //These keep track of coordinates of the ball on the opposite corner on the rectangle
                        double otherX = (double)colorballs.get(otherID).getX();
                        double otherY = (double)colorballs.get(otherID).getY();
                        //need double variants of the maximums and minimums to compare ratios
                        double minWidthD = (double)minWidth;
                        double minHeightD = (double)minHeight;
                        double maxWidthD = (double)maxWidth;
                        double maxHeightD = (double)maxHeight;

                        //Checks to make sure the box won't invert, or go out of bounds.
                        if( balID == 0 && myRatio * (Y - otherY) + otherX > minWidth - ballRadius && myRatio * (Y - otherY) + otherX < colorballs.get(otherID).getX() && Y > minHeight - ballRadius && Y < colorballs.get(otherID).getY() ||
                                balID == 2 && myRatio * (Y - otherY) + otherX < maxWidth - ballRadius && myRatio * (Y - otherY) + otherX > colorballs.get(otherID).getX() && Y < maxHeight - ballRadius && Y > colorballs.get(otherID).getY())
                        {
                            //readability
                            newY = Y;

                            //make newX adhere to the ratio
                            newX = (int)(myRatio * (newY - otherY) + otherX);
                        }
                        //If the box DOES invert, or go out of bounds, and is the top left (balID == 0)
                        else if(balID == 0)
                        {
                            //if the new X coordinate is less than minimum width
                            if( (myRatio * (Y - otherY) + otherX < minWidth - ballRadius) &&
                                    ((otherX - minWidthD)/(otherY - minHeightD)) < myRatio)
                            {
                                newX = minWidth - ballRadius;
                                //make newY adhere to the ratio
                                newY = (int)((newX - otherX)/myRatio + otherY);
                            }
                            //if the new Y coordinate is less than the minimum height
                            else if(Y < minHeight - ballRadius &&
                                    (otherX - minWidthD)/(otherY - minHeightD) > myRatio)
                            {
                                newY = minHeight - ballRadius;
                                //make newX adhere to the ratio
                                newX = (int)(myRatio * (newY - otherY) + otherX);
                            }
                            else
                            {
                                newX = colorballs.get(balID).getX();
                                //make newY adhere to the ratio
                                newY = (int)((newX - otherX)/myRatio + otherY);
                            }
                        }
                        //If the box DOES invert, or go out of bounds, and is the bottom right (balID == 2)
                        else if(balID == 2)
                        {
                            //if the new X coordinate is more than the maximum width
                            if( (myRatio * (Y - otherY) + otherX > maxWidth - ballRadius) &&
                                    (maxWidthD - otherX)/(maxHeightD - otherY) < myRatio)
                            {
                                newX = maxWidth - ballRadius;
                                //make newY adhere to the ratio
                                newY = (int)((newX - otherX)/myRatio + otherY);
                            }
                            //if the new Y coordinate is more than the maximum height
                            else if(Y > maxHeight - ballRadius &&
                                    (maxWidthD - otherX)/(maxHeightD - otherY) > myRatio)
                            {
                                newY = maxHeight - ballRadius;
                                //make newX adhere to the ratio
                                newX = (int)(myRatio * (newY - otherY) + otherX);
                            }
                            else
                            {
                                newX = colorballs.get(balID).getX();
                                //make newY adhere to the ratio
                                newY = (int)((newX - otherX)/myRatio + otherY);
                            }
                        }
                        else
                        {
                            //Shouldnt ever end up here...
                            newX = colorballs.get(balID).getX();
                            //make newY adhere to the ratio
                            newY = (int)((newX - otherX)/myRatio + otherY);
                        }

                        //Set selected ball's new coordinates
                        colorballs.get(balID).setY(newY);
                        colorballs.get(balID).setX(newX);
                        //Set the other Cropbox corner's coordinates relative to the moved corner.
                        colorballs.get(1).setX(colorballs.get(0).getX());
                        colorballs.get(1).setY(colorballs.get(2).getY());
                        colorballs.get(3).setX(colorballs.get(2).getX());
                        colorballs.get(3).setY(colorballs.get(0).getY());
                        colorballs.get(4).setX(((int)otherX + newX)/2);
                        colorballs.get(4).setY(((int)otherY + newY)/2);
                        canvas.drawRect(point1.x, point3.y, point3.x, point1.y, paint);
                    }
                    else if (groupId == 2)//balls 1 or 3
                    {
                        //otherID refers to the id of the ball on the opposite corner of the cropbox
                        int otherID;
                        if(balID == 1)
                        {
                            otherID = 3;
                        }
                        else
                        {
                            otherID = 1;
                        }

                        //These two variables are for readability's sake. The new coordinates of the colorball.
                        int newX, newY;

                        //These keep track of coordinates of the ball on the opposite corner on the rectangle
                        double otherX = (double)colorballs.get(otherID).getX();
                        double otherY = (double)colorballs.get(otherID).getY();
                        //need double variants of the maximums and minimums to compare ratios
                        double minWidthD = (double)minWidth;
                        double minHeightD = (double)minHeight;
                        double maxWidthD = (double)maxWidth;
                        double maxHeightD = (double)maxHeight;

                        //Checks to make sure the box won't invert, or go out of bounds.
                        if( balID == 3 && otherX - myRatio * (Y - otherY) < maxWidth - ballRadius && otherX - myRatio * (Y - otherY) > otherX && Y > minHeight - ballRadius && Y < otherY ||
                                balID == 1 && otherX - myRatio * (Y - otherY) > minWidth - ballRadius && otherX - myRatio * (Y - otherY) < otherX && Y < maxHeight - ballRadius && Y > otherY)
                        {
                            //for readability
                            newY = Y;

                            //make newX adhere to the ratio
                            newX = (int)(otherX - myRatio * (newY - otherY));
                        }
                        //If the box DOES invert, or go out of bounds, and is the top right (balID == 3)
                        else if(balID == 3)
                        {
                            //if new X coordinate is more then maximum width
                            if( otherX + ballRadius + (otherY + ballRadius - Y - ballRadius)*myRatio > maxWidth - ballRadius &&
                                    (maxWidthD - otherX - ballRadius)/(otherY + ballRadius - minHeightD) < myRatio)
                            {
                                newX = maxWidth - ballRadius;
                                //make newY adhere to the ratio
                                newY = ((int)(otherY - (newX - otherX)/(myRatio)));
                            }
                            //if new Y coordinate is less than minimum height
                            else if(Y < minHeight - ballRadius &&
                                    (maxWidthD - otherX)/(otherY - minHeightD) > myRatio)
                            {
                                newY = minHeight - ballRadius;
                                //make newX adhere to the ratio
                                newX = (int)(otherX - myRatio * (newY - otherY));
                            }
                            else
                            {
                                newX = colorballs.get(balID).getX();
                                //make newY adhere to the ratio
                                newY = (int)(otherY - (newX - otherX)/myRatio);
                            }
                        }
                        //If the box DOES invert, or go out of bounds, and is the bottom left (balID == 1)
                        else if(balID == 1)
                        {
                            //if new X coordinate is less than minimum width
                            if( otherX + ballRadius - (Y + ballRadius - otherY - ballRadius)*myRatio < minWidth - ballRadius &&
                                    (otherX + ballRadius - minWidthD)/(maxHeightD - otherY - ballRadius) < myRatio)
                            {
                                newX = minWidth - ballRadius;
                                //make newY adhere to the ratio
                                newY = (int)((otherX - newX)/myRatio + otherY);
                            }
                            //if new Y coordinate is more than maximum height
                            else if(Y + ballRadius > maxHeight - ballRadius &&
                                    (otherX + ballRadius - minWidthD)/(maxHeightD - otherY - ballRadius) > myRatio)
                            {
                                newY = maxHeight - ballRadius;
                                //make newX adhere to the ratio
                                newX = (int)(otherX - myRatio * (newY - otherY));
                            }
                            else
                            {
                                newX = colorballs.get(balID).getX();
                                //make newY adhere to the ratio
                                newY = (int)((otherX - newX)/myRatio + otherY);
                            }
                        }
                        else
                        {
                            //Shouldn't ever end up here...
                            newX = colorballs.get(balID).getX();
                            //make newY adhere to the ratio
                            newY = (int)((newX - otherX)/myRatio + otherY);
                        }

                        colorballs.get(balID).setY(newY);
                        colorballs.get(balID).setX(newX);

                        colorballs.get(0).setX(colorballs.get(1).getX());
                        colorballs.get(0).setY(colorballs.get(3).getY());
                        colorballs.get(2).setX(colorballs.get(3).getX());
                        colorballs.get(2).setY(colorballs.get(1).getY());
                        colorballs.get(4).setX(((int)otherX + newX)/2);
                        colorballs.get(4).setY(((int)otherY + newY)/2);
                        canvas.drawRect(point2.x, point4.y, point4.x, point2.y, paint);
                    }
                    else //the moving ball at centre
                    {
                        /*if(checkXBoundaries(X, ballRadius))
                        {
                            colorballs.get(1).setX(colorballs.get(1).getX() + X - colorballs.get(4).getX());
                            colorballs.get(3).setX(colorballs.get(3).getX() + X - colorballs.get(4).getX());
                            colorballs.get(0).setX(colorballs.get(0).getX() + X - colorballs.get(4).getX());
                            colorballs.get(2).setX(colorballs.get(2).getX() + X - colorballs.get(4).getX());

                            colorballs.get(balID).setX(X);
                        }
                        else if(!checkLeftBoundaries(X, ballRadius))
                        {
                            int xDist = colorballs.get(balID).getX() - colorballs.get(0).getX();

                            colorballs.get(0).setX(minWidth - ballRadius);
                            colorballs.get(1).setX(minWidth - ballRadius);
                            colorballs.get(2).setX(minWidth + 2 * xDist - ballRadius);
                            colorballs.get(3).setX(minWidth + 2 * xDist - ballRadius);
                            colorballs.get(4).setX(minWidth + xDist - ballRadius);
                        }
                        else if(!checkRightBoundaries(X, ballRadius))
                        {
                            int xDist = colorballs.get(balID).getX() - colorballs.get(0).getX();

                            colorballs.get(0).setX(maxWidth - 2 * xDist - ballRadius);
                            colorballs.get(1).setX(maxWidth - 2 * xDist - ballRadius);
                            colorballs.get(2).setX(maxWidth - ballRadius);
                            colorballs.get(3).setX(maxWidth - ballRadius);
                            colorballs.get(4).setX(maxWidth - xDist - ballRadius);
                        }

                        if(checkYBoundaries(Y, ballRadius))
                        {
                            colorballs.get(1).setY(colorballs.get(1).getY() + Y - colorballs.get(4).getY());
                            colorballs.get(3).setY(colorballs.get(3).getY() + Y - colorballs.get(4).getY());
                            colorballs.get(0).setY(colorballs.get(0).getY() + Y - colorballs.get(4).getY());
                            colorballs.get(2).setY(colorballs.get(2).getY() + Y - colorballs.get(4).getY());

                            colorballs.get(balID).setY(Y);
                        }
                        else if(!checkTopBoundaries(Y, ballRadius))
                        {
                            int yDist = colorballs.get(balID).getY() - colorballs.get(0).getY();

                            colorballs.get(0).setY(minHeight - ballRadius);
                            colorballs.get(1).setY(minHeight + 2 * yDist - ballRadius);
                            colorballs.get(2).setY(minHeight + 2 * yDist - ballRadius);
                            colorballs.get(3).setY(minHeight - ballRadius);
                            colorballs.get(4).setY(minHeight + yDist - ballRadius);
                        }
                        else if(!checkBottomBoundaries(Y, ballRadius))
                        {

                            int yDist = colorballs.get(balID).getY() - colorballs.get(0).getY();

                            colorballs.get(0).setY(maxHeight - 2 * yDist - ballRadius);
                            colorballs.get(1).setY(maxHeight - ballRadius);
                            colorballs.get(2).setY(maxHeight - ballRadius);
                            colorballs.get(3).setY(maxHeight - 2 * yDist - ballRadius);
                            colorballs.get(4).setY(maxHeight - yDist - ballRadius);
                        }*/
                    }
                    invalidate();
                }
                else
                {
                    ballRadius = colorballs.get(0).getHeightOfBall()/2;

                    int middleX = firstMoveX + X - firstX;
                    int middleY = firstMoveY + Y - firstY;

                    System.out.println(colorballs.get(4).getX() + " " + X + " " + firstX);

                    if(checkXBoundaries(middleX, ballRadius))
                    {
                        colorballs.get(1).setX(colorballs.get(1).getX() + middleX - colorballs.get(4).getX());
                        colorballs.get(3).setX(colorballs.get(3).getX() + middleX - colorballs.get(4).getX());
                        colorballs.get(0).setX(colorballs.get(0).getX() + middleX - colorballs.get(4).getX());
                        colorballs.get(2).setX(colorballs.get(2).getX() + middleX - colorballs.get(4).getX());

                        colorballs.get(4).setX(middleX);
                    }
                    else if(!checkLeftBoundaries(middleX, ballRadius))
                    {
                        int xDist = colorballs.get(4).getX() - colorballs.get(0).getX();

                        colorballs.get(0).setX(minWidth - ballRadius);
                        colorballs.get(1).setX(minWidth - ballRadius);
                        colorballs.get(2).setX(minWidth + 2 * xDist - ballRadius);
                        colorballs.get(3).setX(minWidth + 2 * xDist - ballRadius);
                        colorballs.get(4).setX(minWidth + xDist - ballRadius);
                    }
                    else if(!checkRightBoundaries(middleX, ballRadius))
                    {
                        int xDist = colorballs.get(4).getX() - colorballs.get(0).getX();

                        colorballs.get(0).setX(maxWidth - 2 * xDist - ballRadius);
                        colorballs.get(1).setX(maxWidth - 2 * xDist - ballRadius);
                        colorballs.get(2).setX(maxWidth - ballRadius);
                        colorballs.get(3).setX(maxWidth - ballRadius);
                        colorballs.get(4).setX(maxWidth - xDist - ballRadius);
                    }

                    if(checkYBoundaries(middleY, ballRadius))
                    {
                        colorballs.get(1).setY(colorballs.get(1).getY() + middleY - colorballs.get(4).getY());
                        colorballs.get(3).setY(colorballs.get(3).getY() + middleY - colorballs.get(4).getY());
                        colorballs.get(0).setY(colorballs.get(0).getY() + middleY - colorballs.get(4).getY());
                        colorballs.get(2).setY(colorballs.get(2).getY() + middleY - colorballs.get(4).getY());

                        colorballs.get(4).setY(middleY);
                    }
                    else if(!checkTopBoundaries(middleY, ballRadius))
                    {
                        int yDist = colorballs.get(4).getY() - colorballs.get(0).getY();

                        colorballs.get(0).setY(minHeight - ballRadius);
                        colorballs.get(1).setY(minHeight + 2 * yDist - ballRadius);
                        colorballs.get(2).setY(minHeight + 2 * yDist - ballRadius);
                        colorballs.get(3).setY(minHeight - ballRadius);
                        colorballs.get(4).setY(minHeight + yDist - ballRadius);
                    }
                    else if(!checkBottomBoundaries(middleY, ballRadius))
                    {

                        int yDist = colorballs.get(4).getY() - colorballs.get(0).getY();

                        colorballs.get(0).setY(maxHeight - 2 * yDist - ballRadius);
                        colorballs.get(1).setY(maxHeight - ballRadius);
                        colorballs.get(2).setY(maxHeight - ballRadius);
                        colorballs.get(3).setY(maxHeight - 2 * yDist - ballRadius);
                        colorballs.get(4).setY(maxHeight - yDist - ballRadius);
                    }
                }

                break;

            case MotionEvent.ACTION_UP:
                // touch drop - just do things here after dropping

                break;
        }
        // redraw the canvas
        invalidate();
        return true;

    }

    private boolean checkXBoundaries(int X, int ballRadius)
    {
        boolean withinBounds = true;
        if( !checkLeftBoundaries(X, ballRadius) || !checkRightBoundaries(X, ballRadius))
        {
            withinBounds = false;
        }
        return withinBounds;
    }

    private boolean checkYBoundaries(int Y, int ballRadius)
    {
        boolean withinBounds = true;
        if( !checkTopBoundaries(Y, ballRadius) || !checkBottomBoundaries(Y, ballRadius))
        {
            withinBounds = false;
        }
        return withinBounds;
    }

    private boolean checkLeftBoundaries(int X, int ballRadius)
    {
        boolean inBounds = true;
        if( colorballs.get(1).getX() + X - colorballs.get(4).getX() < minWidth - ballRadius ||
                colorballs.get(0).getX() + X - colorballs.get(4).getX() < minWidth - ballRadius)
        {
            inBounds = false;
        }
        return inBounds;
    }

    private boolean checkTopBoundaries(int Y, int ballRadius)
    {
        boolean inBounds = true;
        if( colorballs.get(3).getY() + Y - colorballs.get(4).getY() < minHeight - ballRadius ||
                colorballs.get(0).getY() + Y - colorballs.get(4).getY() < minHeight - ballRadius)
        {
            inBounds = false;
        }
        return inBounds;
    }

    private boolean checkRightBoundaries(int X, int ballRadius)
    {
        boolean inBounds = true;
        if( colorballs.get(3).getX() + X - colorballs.get(4).getX() > maxWidth - ballRadius ||
                colorballs.get(2).getX() + X - colorballs.get(4).getX() > maxWidth - ballRadius)
        {
            inBounds = false;
        }
        return inBounds;
    }

    private boolean checkBottomBoundaries(int Y, int ballRadius)
    {
        boolean inBounds = true;
        if( colorballs.get(1).getY() + Y - colorballs.get(4).getY() > maxHeight - ballRadius ||
                colorballs.get(2).getY() + Y - colorballs.get(4).getY() > maxHeight - ballRadius)
        {
            inBounds = false;
        }
        return inBounds;
    }

    //public method to return the coordinates of the corners of the crop screen.
    //Used to actually perform the crop.
    public int[] getCropDetails()
    {
        //Array to be returned
        int[] retArray = new int[6];

        //X coordinate for top left point
        retArray[0] = colorballs.get(0).getX() + ballRadius - minWidth;
        //Y coordinate for top left point
        retArray[1] = colorballs.get(0).getY() + ballRadius - minHeight;
        //Width of crop box
        retArray[2] = colorballs.get(3).getX() - colorballs.get(0).getX();
        //Height of crop box
        retArray[3] = colorballs.get(1).getY() - colorballs.get(0).getY();
        //Maximum Width of the View
        retArray[4] = picWidth;
        //Maximum Height of the View
        retArray[5] = picHeight;

        return retArray;
    }
    /**
     public CustomDrawView(Context context) {
     super(context);
     paint = new Paint();
     setFocusable(true); // necessary for getting the touch events
     canvas = new Canvas();
     // setting the start point for the balls
     point1 = new Point();
     point1.x = 50;
     point1.y = 20;

     point2 = new Point();
     point2.x = 150;
     point2.y = 20;

     point3 = new Point();
     point3.x = 150;
     point3.y = 120;

     point4 = new Point();
     point4.x = 50;
     point4.y = 120;

     // declare each ball with the ColorBall class
     colorballs.add(new ColorBall(context, R.drawable.drag_circle, point1));
     colorballs.add(new ColorBall(context, R.drawable.drag_circle, point2));
     colorballs.add(new ColorBall(context, R.drawable.drag_circle, point3));
     colorballs.add(new ColorBall(context, R.drawable.drag_circle, point4));

     //set Bitmap
     //imgBit = inImgBit;
     }/**/
}
