package curtin.mobile.passportphotocreator;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

import java.lang.reflect.Field;
import java.util.ArrayList;


public class RequirementsCheckActivity extends ActionBarActivity
{
    ArrayList<ImageView> theViews = new ArrayList<ImageView>();
    int screenWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requirements_check);
        final Context context = this;

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;

        setViews();

        ImageView imgView = (ImageView)findViewById(R.id.imageView);

        GlobalImageData data = ((GlobalImageData)getApplicationContext());

        Bitmap displayImage = Bitmap.createScaledBitmap(data.getImage(4), screenWidth/2 - 16, (screenWidth/2 - 16) * data.getImage(4).getHeight() / data.getImage(4).getWidth(), true);

        imgView.setImageBitmap(displayImage);

        /*
        Continue Button
         */
        final Button btnContinue = (Button)findViewById(R.id.continueButton);
        btnContinue.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion) {
                switch (theMotion.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnContinue.setAlpha((float) 0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        btnContinue.setAlpha((float) 1.0);
                        Intent intent = new Intent(context, PrintReadyActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });

        /*
        View Picture Button
         */
        final Button btnView = (Button)findViewById(R.id.viewButton);
        btnView.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion) {
                ImageView imageView = (ImageView) findViewById(R.id.imageView);

                switch (theMotion.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnView.setAlpha((float) 0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        btnView.setAlpha((float) 1.0);
                        if (imageView.getVisibility() == View.GONE) {
                            btnView.setText("View My Image ▲");
                            imageView.setVisibility(View.VISIBLE);
                        } else {
                            btnView.setText("View My Image ▼");
                            imageView.setVisibility(View.GONE);
                        }
                        break;
                }
                return true;
            }
        });

        /*
        Event handler for when the scroll view is changed!
         */
        final CustomScrollView scrollView = (CustomScrollView)findViewById(R.id.scroll);
        scrollView.setOnScrollViewListener(new CustomScrollView.OnScrollViewListener() {
            public void onScrollChanged(CustomScrollView v, int l, int t, int oldl, int oldt)
            {
                for(int i = 0; i < theViews.size(); i++)
                {
                    Rect scrollBounds = new Rect();
                    scrollView.getHitRect(scrollBounds);
                    if (theViews.get(i).getLocalVisibleRect(scrollBounds))
                    {
                        // Any portion of the imageView, even a single pixel, is within the visible window
                        if(theViews.get(i).getVisibility() == View.INVISIBLE)
                        {
                            theViews.get(i).setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        // NONE of the imageView is within the visible window
                        if(theViews.get(i).getVisibility() == View.VISIBLE);
                        {
                            theViews.get(i).setVisibility(View.INVISIBLE);
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_requirements_check, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    puts all the views into an arraylist so they can be checked, and made invisible/visible at will.
     */
    private void setViews()
    {
        Resources r = getResources();
        for(int i = 1; i < 27; i++)
        {
            try
            {
                theViews.add((ImageView) findViewById(r.getIdentifier("IV" + i, "id", getPackageName())));
                //theViews.get(i).buildDrawingCache();
                //theViews.get(i).setImageBitmap(Bitmap.createScaledBitmap(theViews.get(i).getDrawingCache(), screenWidth/3, screenWidth/3*theViews.get(i).getDrawingCache().getHeight()/theViews.get(i).getDrawingCache().getWidth() / 2, true));
                //theViews.get(i).destroyDrawingCache();
            }
            catch(Exception e)
            {
                System.out.println("IV" + i + " doesn't exist.");
            }
        }
    }

    public static int getResId(String resName, Class<?> c)
    {
        try
        {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return -1;
        }
    }
}
