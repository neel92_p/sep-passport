package curtin.mobile.passportphotocreator;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;
import android.widget.GridView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by neel on 28/03/2015.
 */

/**
 * The String passed as the parameter is the path to the images that are to be decoded.
 */
public class FetchImagesAsync extends AsyncTask<GridViewAdapter, Void, Void>
{

    //This is the array that is used to save decoded bitmaps, and is also ArrayList returned.
    ArrayList listImages = new ArrayList();
    GridViewAdapter customAdapter;
    private static final String DEBUG_TAG = "FetchImagesAsync";



    @Override
    protected Void doInBackground(GridViewAdapter... views)
    {
        GridViewAdapter gridView = views[0];  //Get the GridViewAdapter passed in.

        //String directoryLocation = context.getString(R.string.directory);

        //Directory location is hardcoded for the time-being while an alternative is thought of.
        String directoryLocation = "/sdcard/PassportPhotoApps/Images";

        //Open the directory where all the pictures are located.
        File file = new File(directoryLocation);
        //Get all files in the directory
        File[] list = file.listFiles();
        int count = 1;
        //For every file in the list, open if it is a .png and insert in the array imageItems.
        for (File f: list)
        {

            String name = f.getName();
            ImageManipulation imgManipulation = new ImageManipulation();
            //Only read the images ending with a .png
            if (name.endsWith(".png")) {

                String absolutePath = f.getAbsolutePath();


                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, options);

                if (bitmap != null) {
                    //Rotate the image so that it is displayed as portrait
                    bitmap = imgManipulation.rotateImage(bitmap, 90);


                    ImageItem imageItem = new ImageItem(bitmap, name);
                    gridView.add(imageItem);
                }

            }


        }

        return null;
    }


    @Override
    protected void onPreExecute()
    {
        //Do nothing...
    }


    protected void onPostExecute()
    {

    }

    @Override
    protected void onCancelled()
    {
        //Log that it was cancelled.
        Log.i(DEBUG_TAG, "onCancelled");
    }



}
