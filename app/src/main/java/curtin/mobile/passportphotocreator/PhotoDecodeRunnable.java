package curtin.mobile.passportphotocreator;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.*;

import java.io.File;
import java.util.ArrayList;
import android.content.Context;


//THIS CLASS IS CURRENTLY USELESS. NOT USED. UPDATED: 28/3/2015 11.17PM.

/**
 * Created by neel on 27/03/2015.
 *
 * This class is used to setup a multi-threaded way to decode images to bitmaps so that it is
 * faster to decode images so that the main UI does not slow down.
 */
public class PhotoDecodeRunnable implements Runnable {


    private ArrayList listImages = new ArrayList();
    File [] files;

    //A function to read files from the directory folder.

    public PhotoDecodeRunnable()
    {
        //do nothing.
    }

    /**
     * Function reads files from the directory folder.
     * @param path This path is the directory location in which images to be read are stored.
     * @return Array for bitmaps.
     */
    public ArrayList readFilesFromDirectory (String path) {
        //Get the location of the directory where all captured images are stored.

        File file = new File(path);
        //Get all files in the directory
        files = file.listFiles();



        int count = 1;
        //For every file in the list, open if it is a .png and insert in the array imageItems.
        for (File f : files) {

            String name = f.getName();
            ImageManipulation imgManipulation = new ImageManipulation();
            //Only read the images ending with a .png
            if (name.endsWith(".png")) {

                String absolutePath = f.getAbsolutePath();


                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, options);

                if (bitmap != null) {
                    //Rotate the image so that it is displayed as portrait
                    bitmap = imgManipulation.rotateImage(bitmap, 90);


                    listImages.add(new ImageItem(bitmap, name));
                }

            }

        }

        /*
        run();
        return listImages;
        */

        return listImages;
    }


    public void run()
    {
        // Ensure the thread runs in the background, so that it does not compete with the main UI thread.
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        for (File f: files)
        {
            String name = f.getName();
            ImageManipulation imgManipulation = new ImageManipulation();
            //Only read the images ending with a .png
            if (name.endsWith(".png")) {

                String absolutePath = f.getAbsolutePath();

                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, options);

                if (bitmap != null) {
                    //Rotate the image so that it is displayed as portrait
                    bitmap = imgManipulation.rotateImage(bitmap, 90);


                    listImages.add(new ImageItem(bitmap, name));
                }

            }
        }


    }

}
