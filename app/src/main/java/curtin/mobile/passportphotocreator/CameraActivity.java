package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.hardware.*;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CameraActivity extends Activity implements PictureCallback, SurfaceHolder.Callback, SensorEventListener
{

    public static final String EXTRA_CAMERA_DATA = "camera_data";

    private static final String KEY_IS_CAPTURING = "is_capturing";
    private static final String FLASH_ON = "Flash On";
    private static final String FLASH_OFF = "Flash Off";
    private static final String FLASH_AUTO = "Flash Auto";

    private Camera mCamera;
    private ImageView mCameraImage;
    private SurfaceView mCameraPreview;
    private ImageButton mCaptureImageButton;
    private ImageButton invertCameraButton;
    private byte[] mCameraData;
    private boolean mIsCapturing;
    private ImageView mFlash;
    private TextView flashText;
    private ImageButton infoButton;
    private static int flashButtonCount = 0;
    private int currentCameraId;
    private final int FOCUS_AREA_SIZE = 300;
    private TextView tiltStatus;
    private Sensor mAccelerometer;
    private SensorManager mSensorManager;

    private Menu menu;


//    private OnClickListener mCaptureImageButtonClickListener = new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//
//            mCaptureImageButton.setImageResource(R.drawable.camera_clicked);
//            mCaptureImageButton.setImageResource(R.drawable.camera_unclicked);
//            captureImage();
//        }
//    };

    //This is the button onTouchListener for the capture button.
    private View.OnTouchListener mCaptureImageButtonTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction())
            {
                //When the button is pressed down, the button icon changes to display interactions.
                case MotionEvent.ACTION_DOWN:
                {
                    mCaptureImageButton.setBackgroundResource(R.drawable.camera_clicked);
                    break;
                }
                //When the button is pressed up, the button icon changes to display interactions.
                case MotionEvent.ACTION_UP:
                {
                    //mCaptureImageButton.setImageResource(R.drawable.camera_unclicked);
                    mCaptureImageButton.setBackgroundResource(R.drawable.camera_unclicked);
                    //Take a picture after the button is released.
                    captureImage();
                    break;
                }
            }
            return true;
        }
    };

    private OnClickListener mRecaptureImageButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            setupImageCapture();
        }
    };



    //This is used to save the image when the activity ends.
//    private OnClickListener mDoneButtonClickListener = new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            if (mCameraData != null) {
//                Intent intent = new Intent();
//                intent.putExtra(EXTRA_CAMERA_DATA, mCameraData);
//                setResult(RESULT_OK, intent);
//            } else {
//                setResult(RESULT_CANCELED);
//            }
//            finish();
//        }
//    };


    //This is the listener for the invert camera button. It is used to swap the cameras.
    private OnClickListener invertCameraButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            //Stop the display of the current camera. Then release the camera and swap to alternative.
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            }

            //Find out the current camera on use. And swap with the alternative.
            if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
            else
                currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;

            //Get the camera live feed and display it to the user.
            try {

                //Start the new camera. The setDisplayOrientation keeps it at a portrait mode.
                mCamera = Camera.open(currentCameraId);
                mCamera.setDisplayOrientation(90);
                mCamera.setPreviewDisplay(mCameraPreview.getHolder());
                mCamera.startPreview();

            }
            catch (Exception e) { e.printStackTrace(); }
        }
    };


    //This is an onTouchListener for the preview space. Clicking on it is used to focus the camera on the area clicked.
    private View.OnTouchListener mCameraPreviewTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (mCamera != null) {

                mCamera.cancelAutoFocus();
                //Get the focus coordinates.
                Rect focusRect = calculateFocusArea(event.getX(), event.getY());

                //Set the camera focus parameters.
                Camera.Parameters parameters = mCamera.getParameters();
                if (parameters.getFocusMode() != Camera.Parameters.FOCUS_MODE_AUTO) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                }

                //Tell the camera the focus area. This is where the focus is to be placed.
                List<Camera.Area> mylist = new ArrayList<Camera.Area>();
                mylist.add(new Camera.Area(focusRect, 1000));
                parameters.setFocusAreas(mylist);

                try {
                    //Manual focus only works after disabling the auto-focus.
                    mCamera.cancelAutoFocus();
                    mCamera.setParameters(parameters);
                    mCamera.startPreview();
                    //AutoFocusCallBack is used to automatically enter auto-focus if the function is called.
                    //It puts it in continuous focus.
                    mCamera.autoFocus(new Camera.AutoFocusCallback() {
                        @Override
                        public void onAutoFocus(boolean success, Camera camera) {
                            if (camera.getParameters().getFocusMode() != Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) {
                                Camera.Parameters parameters = camera.getParameters();
                                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                                parameters.setFocusAreas(null);
                                camera.setParameters(parameters);
                                camera.startPreview();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return true;

        }
    };


    //Function used to calculate the coordinates of the camera to focus to.
    //A graphic element of the type rectangle is returned with the coordinates.
    private Rect calculateFocusArea(float x, float y) {
        int left = clamp(Float.valueOf((x / mCameraPreview.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
        int top = clamp(Float.valueOf((y / mCameraPreview.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

        return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
            if (touchCoordinateInCameraReper>0){
                result = 1000 - focusAreaSize/2;
            } else {
                result = -1000 + focusAreaSize/2;
            }
        } else{
            result = touchCoordinateInCameraReper - focusAreaSize/2;
        }
        return result;
    }

    /**
     * Click Listener for the flash button.
     * Every time the button is pressed, the flash is either set to auto, on or off.
     */
    private OnClickListener mFlashButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //The flashButtonCount is used to determine whether the flash is to be set on
            //auto, on or off.
            flashButtonCount++;
            flashButtonCount = (flashButtonCount % 3);
            if (mCamera != null)
            {

                Camera.Parameters params = mCamera.getParameters();

                switch(flashButtonCount)
                {
                    case 0:
                    {
                        //Camera flash is set to on.
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                        //Change the text of the flash to ON when flash is set to on.
                        flashText.setText("AUTO");
                        break;

                    }
                    case 1:
                    {
                        //Camera flash is set to off.
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        //Change the text on the screen to indicate flash mode is on off.
                        flashText.setText("OFF");
                        break;
                    }
                    case 2:
                    {
                        //Camera flash is set to auto.
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        //Change the text on the screen to indicate flash mode is on auto.
                        flashText.setText("ON");
                        break;
                    }
                }

                mCamera.setParameters(params);
            }
        }
    };

    //This shows the guidelines to take pictures.
    private OnClickListener infoButton_clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            startGuidelinesActivity();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_camera);

        mCameraImage = (ImageView) findViewById(R.id.camera_image_view);
        mCameraImage.setVisibility(View.INVISIBLE);

        mCameraPreview = (SurfaceView) findViewById(R.id.preview_view);
        final SurfaceHolder surfaceHolder = mCameraPreview.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        //Set the onTouchListener. This listener is used to create a focus on area for the camera
        //when the screen is touched. Read mCameraPreviewTouchListener for more information.
        mCameraPreview.setOnTouchListener(mCameraPreviewTouchListener);

        mCaptureImageButton = (ImageButton) findViewById(R.id.captureImageButton);
        //mCaptureImageButton.setOnClickListener(mCaptureImageButtonClickListener);
        mCaptureImageButton.setOnTouchListener(mCaptureImageButtonTouchListener);

        mFlash = (ImageView) findViewById(R.id.flash_button);
        mFlash.setOnClickListener(mFlashButtonClickListener);
        flashText = (TextView) findViewById(R.id.flash_status);

        infoButton = (ImageButton) findViewById(R.id.info_button);
        infoButton.setOnClickListener(infoButton_clickListener);

        invertCameraButton = (ImageButton) findViewById(R.id.invert_camera_button);
        invertCameraButton.setOnClickListener(invertCameraButtonClickListener);

        //Set up the gyroscope to make and start taking readings of the tilt of the camera.
        //The gyroscope is used to make sure the camera is in portrait mode and the phone is as straight
        //as possible.
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mIsCapturing = true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putBoolean(KEY_IS_CAPTURING, mIsCapturing);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //When the activity is restored, start the camera again.

        mIsCapturing = savedInstanceState.getBoolean(KEY_IS_CAPTURING, mCameraData == null);
        if (mCameraData != null) {
            setupImageDisplay();
        } else {
            setupImageCapture();
        }
    }


    /***
     * onResume() is what happens when the activity is paused for a while. and the starts back again.
     */
    @Override
    protected void onResume() {
        super.onResume();


        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String outlineImageName = dimens.getOutline(country);

        if (outlineImageName == null)
        {
            outlineImageName = "european.png";
        }
        int resourceId = getResources().getIdentifier(outlineImageName, "drawable",
                getPackageName());

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        surfaceView.setBackgroundResource(resourceId);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);

        if (mCamera == null) {
            try {
                //Open camera

                int numCameras = Camera.getNumberOfCameras();

                //This opens a back facing camera.
                mCamera = Camera.open();

                if (mCamera == null)
                {
                    //Find out the current camera on use. And swap with the alternative.
                    if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                    {
                        currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    }
                    else
                    {
                        currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                    }

                    mCamera.open(currentCameraId);

//                    //If no back-facing camera found, open the front camera.
//                    if (numCameras == 0)
//                    {
//                        return;
//                    }
//
//                    Camera.CameraInfo info = new Camera.CameraInfo();
//                    for (int ii=0; ii <= numCameras; ii++)
//                    {
//                        Camera.getCameraInfo(ii, info);
//
//                        //0 means front-facing and 1 means back-facing camera.
//                        if (info.facing == 0 || info.facing == 1)
//                        {
//
//                            mCamera.open(0);
//
//                        }
//                        if (mCamera != null)
//                        {
//                            break;
//                        }
//
//
//                    }
                }
                //Set auto-focus mode on it.
                Camera.Parameters params = mCamera.getParameters();

                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);

                //If the user has any resolution preferences set in the settings, use that
                //resolution to take the capture.
                SharedPreferences settings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
                String resolution = settings.getString("settings_resolution", "default");

                //If the user has not selected any resolution, the "default" is returned as resolution.
                //If it is default do not do anything, in which case the default resolution is used.
                //Otherwise resolution is returned in the format "width x height". Extract the values
                //and set the resolution for the camera.

                if (!resolution.equals("default"))
                {
                    String[] tokens = resolution.split(" x ");
                    //Resolution is set in the format "width x height".
                    //So first string is width and then height.
                    if (tokens.length >= 2)
                    {
                        int width = Integer.parseInt(tokens[0]);
                        int height = Integer.parseInt(tokens[1]);
                        params.setPreviewSize(width, height);

                    }
                }

                mCamera.setParameters(params);
                //Get the preview on the screen.
                mCamera.setPreviewDisplay(mCameraPreview.getHolder());

                //Rotate image to make it portrait mode.
                mCamera.setDisplayOrientation(90);
                //Start preview.
                if (mIsCapturing) {
                    mCamera.startPreview();
                }
            } catch (Exception e) {
                Toast.makeText(CameraActivity.this, "Unable to open camera.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
        }
    }


    /**
     * When activity enters a pause, release the camera so that other applications may use it.
     * However, it is requested once the activity resumes.
     */
    @Override
    protected void onPause() {
        super.onPause();

        mSensorManager.unregisterListener(this);

        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        mCameraData = data;

        //This function gets the picture and displays it in the current activity and the user can either
        //continue with this picture or take another one. But it does so by using the camera surfaceView to
        //do it. We want this to be changed so that another activity will display the preview of the image captured, with
        //a button to help the user take another picture in which case the previous image is discarded.
        //setupImageDisplay();

        //This function call will handle the image saving. But it does not make any UI changes yet. So the user
        //is not aware of what is going on.
        startPreviewOfCapturedImage();

    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(holder);
                if (mIsCapturing) {
                    mCamera.startPreview();
                }
            } catch (IOException e) {
                Toast.makeText(CameraActivity.this, "Unable to start camera preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    private void captureImage() {
        mCamera.takePicture(null, null, this);
    }

    private void setupImageCapture()
    {
        mCameraImage.setVisibility(View.INVISIBLE);
        mCameraPreview.setVisibility(View.VISIBLE);
        mCamera.startPreview();
        // mCaptureImageButton.setText(R.string.capture_image);
        // mCaptureImageButton.setOnClickListener(mCaptureImageButtonClickListener);
        mCaptureImageButton.setOnTouchListener(mCaptureImageButtonTouchListener);
    }

    private void setupImageDisplay()
    {
        Bitmap bitmap = BitmapFactory.decodeByteArray(mCameraData, 0, mCameraData.length);
        mCameraImage.setImageBitmap(bitmap);
        mCamera.stopPreview();
        mCameraPreview.setVisibility(View.INVISIBLE);
        mCameraImage.setVisibility(View.VISIBLE);
        //mCaptureImageButton.setText(R.string.recapture_image);
        mCaptureImageButton.setOnClickListener(mRecaptureImageButtonClickListener);
    }

    private void startPreviewOfCapturedImage()
    {
        //Get the data captured from the camera and saved as a bitmap.
        Bitmap bitmap = BitmapFactory.decodeByteArray(mCameraData, 0, mCameraData.length);
        //This call will save the bitmap to our pictures directory in .png format.

        //If the image was taken using the front camera, rotate it by 180 degrees so that it is saved
        // as a portrait photo. O
        if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            ImageManipulation manipulation = new ImageManipulation();
            bitmap = manipulation.rotateImage(bitmap, 180);
        }

        String fileCreated  = saveCanvasImage(bitmap);

        //Start intent to the Camera_Preview. Add the filename as an extra to the intent.
        Intent intent = new Intent(this, Camera_Preview.class);
        //Pass in the name of the filename of the image last captured.
        intent.putExtra("LAST_PICTURE_FILENAME", fileCreated);
        //Stop current activity and move to next activity
        finish();
        startActivity(intent);

    }


    /***
     * This function makes a .png out of a bitmap and saves it inside the default directory.
     * @param bitmap
     */

    public String saveCanvasImage(final Bitmap bitmap) {


        //If the user has any resolution preferences set in the settings, use that
        //resolution to take the capture.
        final SharedPreferences settings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        final String resolution = settings.getString("settings_resolution", "default");

        //Get the location of the directory that stores all the application images.
        String directoryLocation = getString(R.string.directory);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_mm_dd_hh_mm",
                Locale.getDefault());

        //The image name is created using the date and time as part of the name. The images are saved as a .jpg.
        final File filename = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + directoryLocation + File.separator + "capture_" + dateFormat.format(new Date()) + ".jpg");

        final int cameraID = currentCameraId;


        //Compress the bitmap and convert it to jpeg in a new thread to increase the speed of doing it.
        Thread t = new Thread()
        {
            public void run()
            {
                Bitmap compressedBitmap = bitmap;

                //If the user has not selected any resolution, the "default" is returned as resolution.
                //If it is default do not do anything, in which case the default resolution is used.
                //Otherwise resolution is returned in the format "width x height". Extract the values
                //and set the resolution for the camera.
                try
                {
                    if ((!resolution.equals("default")) && (cameraID == Camera.CameraInfo.CAMERA_FACING_BACK))
                    {
                        String[] tokens = resolution.split(" x ");
                        //Resolution is set in the format "width x height".
                        //So first string is width and then height.
                        if (tokens.length >= 2)
                        {
                            int width = Integer.parseInt(tokens[0]);
                            int height = Integer.parseInt(tokens[1]);
                            compressedBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);

                        }
                    }
                }

                catch (IllegalArgumentException e)
                {
                    e.printStackTrace();
                }



                try {

                    //The FileOutputSteam is used to write to file and then closed to save.
                    FileOutputStream fos = new FileOutputStream(filename);

                    compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                    fos.flush();

                    fos.close();

                }catch (IOException e){

                    e.printStackTrace();
                }
            }
        };
        t.start();

        //Wait for the thread to finish so that the filename returned by the function actually has data in it.



        try {
            t.join();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return filename.getName();

    }


    /**
     * This function updates the activity with the accelerometer reading in order to determine
     * if there is a tilt or not.
     * This reading is done in real time and the user gets an instant feedback.
     * @param event is the reading on the accelerometer.
     */
    @Override
    public void onSensorChanged(SensorEvent event)
    {
        float y = event.values[1];
        //The tilt status is used to check if the camera is held as straight as possible.
        if (tiltStatus != null)
        {
            //Indicate that a tilt is not detected if the phone is not in perfect portrait mode.
            if (y >= 9.9 && y <= 10.0)
            {
                //If there is no tilt, show a green text
                tiltStatus.setText("No Tilt Detected.");
                //tiltStatus.setBackgroundColor(Color.GREEN);
                tiltStatus.setTextColor(Color.GREEN);
            }
            else
            {
                //If there is a tilt that is detected, then display a red text showing the user.
                tiltStatus.setText("Tilt Detected.");
                tiltStatus.setTextColor(Color.RED);
            }
        }
        else
        {
            tiltStatus = (TextView) findViewById(R.id.tilt);
        }
    }

    //This function is not used. It is just a requirement for SensorEventListener interface.
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }
    /**
     * This starts the activity that displays the guidelines to the user.
     */
    public void startGuidelinesActivity()
    {
        Intent intent = new Intent(this, Guidelines.class);
        startActivity(intent);
    }
}

