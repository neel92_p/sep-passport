package curtin.mobile.passportphotocreator;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


//THIS CLASS IS CURRENTLY USELESS. NOT USED. UPDATED: 28/3/2015 11.17PM.

/**
 * Created by neel on 27/03/2015.
 *
 * This class is used to setup a multi-threaded way to decode images to bitmaps so that it is
 * faster to decode images so that the main UI does not slow down.
 */

public class PhotoDecoder
{
    //A single instance of PhotoDecoder to implement the singleton pattern.
    private static PhotoDecoder instance = null;

    //The cachedPool will run the tasks that are passed to it.
    private ExecutorService decodeThreadPool;

    //A queue of Runnables for the image decoding pool.
    //private final BlockingQueue<Runnable> decodeQueue;

    //A linkedList that holds the bitmaps after the they are decoded by threads.
    //Make sure accessing this bitmap is thread-safe.
    private ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();

    private ArrayList<PhotoTask> tasks;

    /*
    static
    {
        //Create a single static instance of PhotoDecodeThreadPool.
        instance = new PhotoDecoder();
    }
    */

    //Constructor
    public PhotoDecoder()
    {
        //We create a cachedThreadPool. This ensures that any previously available
        //treads will be used to run new task when available. If none are available,
        //then a new thread is created to run the task.
        decodeThreadPool = Executors.newCachedThreadPool();

        //Create a work queue for the pool of Thread objects used for decoding, using a
        //linked list queue that blocks when queue is empty.
        //decodeQueue = new LinkedBlockingQueue<Runnable>();

        //Instantiate the arraylist to return images.
        //bitmapArray = new ArrayList<Bitmap>();

    }

    /**
     * This function adds the task to the threadPool
     * @param inTaskList is a ArrayList of PhotoTasks.
     */
    public void addTasks (ArrayList<PhotoTask> inTaskList)
    {

        this.tasks = inTaskList;
        try
        {
            for (PhotoTask task : tasks)
            {
                decodeThreadPool.execute(task);
                //Future<?> runnableFuture = decodeThreadPool.submit(task);

                /*
                if (runnableFuture.isDone())
                {
                    synchronized (bitmapArray) {
                        bitmapArray.add(task.getBitmap());
                    }
                }
                */
            }
            decodeThreadPool.shutdown();
            decodeThreadPool.awaitTermination(1, TimeUnit.MINUTES);
        }
        catch (Exception e)
        {
            Log.i("PHOTO_DECODE_ADD_TASKS", "exception" + e.getMessage());
        }

    }

    /**
     * This returns an ArrayList of the PhotoTasks after they are processed.
     * @return
     */
    public ArrayList<PhotoTask> getResult()
    {
        ArrayList <PhotoTask> returnList = null;
        if (decodeThreadPool.isShutdown())
        {
            returnList = this.tasks;
        }
        return returnList;
    }

}
