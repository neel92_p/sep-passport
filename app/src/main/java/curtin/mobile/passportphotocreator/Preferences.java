package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.widget.Spinner;


import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class Preferences extends PreferenceActivity {
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor sharedPrefEditor;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setupSimplePreferencesScreen();
        sharedPref = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();

        setListPreferenceData((ListPreference) findPreference("resolution_list"));

    }

    /**
     * Save the changes in the settings and put it in the persistent
     * sharedPreferences so that it can be accessed from other activities.
     */
    private void setSharedPreferences ()
    {
        //Set the current value in the sharedPreferences so that it can be accessed by other activities.
        Preference countryPreferences = findPreference("country_list");
        String countrySummary = ((ListPreference) countryPreferences).getValue();

        //If the country summary is not set after the first initial setting screen.
        if (countrySummary.equals("-1"))
        {
            countrySummary = sharedPref.getString("settings_country", "Australia");
        }

        sharedPrefEditor.putString("settings_country", countrySummary);
        sharedPrefEditor.commit();

        //Set the resolution set by the user so that it can be used by other activities. Saved in the
        //sharedPreferences.
        Preference resolutionPreference = findPreference("resolution_list");
        String resolutionSummary = ((ListPreference) resolutionPreference).getValue();

        sharedPrefEditor.putString("settings_resolution", resolutionSummary);
        sharedPrefEditor.commit();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        //Save the changes of the country everytime we exit.
        setSharedPreferences();
        finish();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        //Save the changes of the country everytime we exit.
        setSharedPreferences();
        finish();

    }

    //Finish the activity when the back button is closed.
    @Override
    public void onBackPressed() {
        finish();
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.

        // Add 'general' preferences.
        addPreferencesFromResource(R.xml.pref_general);


        // Bind the summaries of EditText/List/Dialog/Ringtone preferences to
        // their values. When their values change, their summaries are updated
        // to reflect the new value, per the Android Design guidelines.
        //bindPreferenceSummaryToValue(findPreference("example_text"));
        bindPreferenceSummaryToValue(findPreference("country_list"));
        bindPreferenceSummaryToValue(findPreference("resolution_list"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                || !isXLargeTablet(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBuildHeaders(List<Header> target) {
        if (!isSimplePreferences(this)) {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            }

            else
            {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }

            return true;

        }
    };


    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */

    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.

            bindPreferenceSummaryToValue(findPreference("country_list"));
            bindPreferenceSummaryToValue(findPreference("resolution_list"));
        }
    }


    /**
     * This function simply fills the Camera Resolution spinner with the resolutions supported
     * by the camera. These resolutions are those supported by the back camera.
     * @param lp is the ListPreference which is the spinner to select the resolution.
     */
    protected static void setListPreferenceData(ListPreference lp) {


        if (lp == null)
        {
            return;
        }
        Camera camera = Camera.open();
        List<Camera.Size> list = null;

        //Open camera and get list of supported resolutions.
        if (camera != null)
        {
            Camera.Parameters parameters = camera.getParameters();

            list = parameters.getSupportedPreviewSizes();
        }
        else
        {
            CharSequence[] entries = new CharSequence[1];
            entries[0] = "0 x 0";

            return;
        }

        //Get all the sizes and display them in the spinner (drop-down).
        CharSequence[] entries = new CharSequence[list.size()];
        int ii = 0;
        for (Camera.Size size : list)
        {
            entries[ii] = size.width + " x " + size.height;
            ii++;
        }



        lp.setEntries(entries);
        //The default is set to the best resolution offered by the camera.

        lp.setEntryValues(entries);
        if (camera != null) {
            camera.release(); //Release camera immediately.
        }
    }




}
