package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;


public class Preview extends ActionBarActivity {

    private String selectedImagePath;
    private static final int SELECT_PICTURE = 1;
    FrameLayout layout;

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_blue_red);



        //Set up for dimming with popup.
        layout = (FrameLayout) findViewById( R.id.mainmenu);
        //Dim level is set at 0. when a popup happens, the background will be dimmed. looks cool as hell.
        layout.getForeground().setAlpha(0);

        //Get any data passed in via putExtra.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String imagePath = null;
        if (extras != null)
        {
            imagePath = extras.getString("IMAGE_PATH");
        }

        retrieveImage(imagePath);
        selectedImagePath = imagePath;

        //This is the listener for the button used to select another image if the user doesn't like the one currently chosen.
        ((Button) findViewById(R.id.retakeButton))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        ImageView imageView = (ImageView) findViewById(R.id.imageView);
                        imageView.setBackgroundResource(0);
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                    }
                });

        //This is the listener for the yes button that marks the user is happy with the current image.
        final Context context = this;
                ((Button) findViewById(R.id.yesButton))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (selectedImagePath != null) {
                            //Start a new intent and move to next activity.
                            //Make sure the path selectedImagePath is passed as an extra using putExtra();
                            startImageEditing();
                        }
                        else
                        {
                            System.out.println("Path is null");
                        }
                    }
                });
    }

    /*
    Show a pop up asking if user wants to correct distortion to image.
    If user selects yes, go on to Distortion activity.
    If user selects no, go on to image editing activity.
     */
    private void startImageEditing()
    {
        //Create the pop up
        LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.to_distortion_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                AbsoluteLayout.LayoutParams.WRAP_CONTENT,
                AbsoluteLayout.LayoutParams.WRAP_CONTENT);

        //Dim background
        layout.getForeground().setAlpha(180);

        final Button btnYes = (Button)popupView.findViewById(R.id.yes_button);
        final Button btnNo = (Button)popupView.findViewById(R.id.no_button);

        final Context context = this;
        //What happens if Yes button is pressed
        btnYes.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion) {
                switch (theMotion.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnYes.setAlpha((float) 0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        popupWindow.dismiss();
                        btnYes.setAlpha((float) 1.0);
                        layout.getForeground().setAlpha(0);

                        Intent intent = new Intent(context, DistortionActivity.class);
                        intent.putExtra("IMAGE_PATH", selectedImagePath);
                        System.out.println("Test line before intent");
                        startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });

        //What happens if Next button is pressed.
        btnNo.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion) {
                switch (theMotion.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnNo.setAlpha((float) 0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        btnNo.setAlpha((float) 1.0);
                        popupWindow.dismiss();
                        layout.getForeground().setAlpha(0);

                        String imagePath = createImageFromBitmap(BitmapFactory.decodeFile(selectedImagePath));

                        Intent intent = new Intent(context, ImageEditingActivity.class);
                        intent.putExtra("IMAGE", imagePath);
                        System.out.println("Test line before intent");
                        startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });

        //Show the Window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, -1, -1);
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                if (selectedImagePath != null) {
                    System.out.println("start preview activity \n \n \n\n\n ATTENTION");
                    startPreviewActivity();

                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_preview, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }

    /*
    * Reason for this onBackPressed is to override what the system does when the back button on the
    * device is clicked. In this case, nothing. This means that i've disabled the back button on this
    * activity so that the user is able to choose the image from the gallery again.
     */
    /*
    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    /**
     * This function is called to create a preview of the image and put it in the
     * preview box.
     * @param path - path of the image selected.
     */
    public void retrieveImage (String path)
    {

        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        if (path != null) {
            ImageManipulation imgManipulation = new ImageManipulation();
            //The 300 is the width required. The 400 is the height of the image after the
            //resolution has been reduced.
            Bitmap lowerResolutionImage = imgManipulation.lessResolution(path, 480, 640);
            //Rotate the picture to make it portrait.
            //lowerResolutionImage = imgManipulation.rotateImage(lowerResolutionImage, 90);
            //Use the lowerResolutionImage is displayed on the imageView of this activity.
            imageView.setImageBitmap(lowerResolutionImage);
        }

    }

    /**
     * This function starts the "Preview" activity when the function is called.
     */
    public void startPreviewActivity ()
    {
        //Param imagePath is the path to the image selected by the user,
        //or the path of the photo recently taken.
        //Create an intent to start the application.
        Intent intent = new Intent (this, Preview.class);

        //Set extra information via put extra. Send the path to the image (selectedImagePath).
        if (selectedImagePath != null)
        {
            intent.putExtra("IMAGE_PATH", selectedImagePath);
            finish(); //Close the current activity and start a new one.
            startActivity(intent);
        }


    }

    /**
     * Returns to the path of an image URI
     *
     */
    public String getPath (Uri uri)
    {

        String [] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            return picturePath;
        }

        //This is our fallback here.
        return uri.getPath();


    }


    public String createImageFromBitmap(Bitmap bitmap)
    {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }
}
