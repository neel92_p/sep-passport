package curtin.mobile.passportphotocreator;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by neel on 26/03/2015.
 */
public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    //This array holds an aray of images.
    private ArrayList data = new ArrayList();

    public GridViewAdapter(Context context, int layoutResourceId,
                           ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }


    //Get view returns every row of images to be displayed in the grid view that is used to display previous pictures.
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageTitle = (TextView) row.findViewById(R.id.text);
            holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ImageItem item = (ImageItem) data.get(position);
        holder.imageTitle.setText(item.getTitle());
        holder.image.setImageBitmap(item.getImage());
        return row;
    }

    //Add is used to add pictures to the grid-view adapter.
    public void add(Bitmap newImage, String name)
    {
        //Add the picture to the array
        data.add(newImage);
        //Notify the view that it needs to be refreshed.
        notifyDataSetChanged();
    }

    //Static class that holds an imageView and a TextView.
    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }
}
