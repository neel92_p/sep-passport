package curtin.mobile.passportphotocreator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class RotationActivity extends ActionBarActivity
{

    int rotationValue = 0;
    Bitmap lowResImage;

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotation);

        final GlobalImageData data = ((GlobalImageData)getApplicationContext());
        final ImageView imageView = (ImageView) findViewById(R.id.imagePreview);
        lowResImage = Bitmap.createScaledBitmap(data.getImage(2),
                data.getImage(2).getWidth()/3, data.getImage(2).getHeight()/3, true);
        imageView.setImageBitmap(lowResImage);

        final SeekBar rotateBar = (SeekBar)findViewById(R.id.rotateBar);
        rotateBar.setMax(90);
        rotateBar.setProgress(45);
        final TextView rotateValue = (TextView)findViewById(R.id.rotateValue);


        final ImageButton rotate90Button = (ImageButton)findViewById(R.id.rotate90Button);
        rotate90Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Bitmap rotatedLowResImage = rotateBitmap(lowResImage, 90);
                rotationValue = (rotationValue + 90)%360;
                rotateBar.setProgress(45);
                rotateValue.setText(String.valueOf((float)(360 + rotateBar.getProgress() - 45 + rotationValue)%360));
                lowResImage = rotatedLowResImage;
                imageView.setImageBitmap(rotatedLowResImage);
            }
        });



        //This is the listener used for scrolling the rotation seekbar. It changes the
        //rotation of the low res image when moved
        rotateBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser)
            {
                rotateValue.setText(String.valueOf((float)(360 + rotateBar.getProgress() - 45 + rotationValue)%360));
                imageView.setImageBitmap(rotateBitmap(lowResImage, (float)(progress - 45)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });

        //This is the listener for the button used to reset the rotation back to default
        (findViewById(R.id.resetButton))
                .setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        rotateBar.setProgress(45);
                        imageView.setImageBitmap(rotateBitmap(lowResImage, (float)(0)));
                    }
                });

        final Context context = this;
        //This is the listener for the button used to go to the next screen, presumably cropping
        (findViewById(R.id.nextButton))
                .setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        //Start a new intent and move to next activity.
                        if(data.getImage(2) != null)
                        {
                            data.updateImageThree(rotateBitmap(data.getImage(2),
                                    (float)(rotateBar.getProgress() - 45 + rotationValue)));
                            Intent intent = new Intent(context, CropActivity.class);
                            startActivity(intent);
                            /*
                             *END Heading to Crop Activity
                             */
                        }
                    }
                });
    }

    private int getRotationValue()
    {
        this.rotationValue = this.rotationValue + 90;
        return this.rotationValue;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_rotation, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }

    public Bitmap rotateBitmap(Bitmap source, float angle)
    {
        System.gc();
        Matrix matrix = new Matrix();
        matrix.postRotate(-angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

}
