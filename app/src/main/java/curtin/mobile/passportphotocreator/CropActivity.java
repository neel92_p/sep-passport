package curtin.mobile.passportphotocreator;
/****
 * CREATOR: TIMOTHY SMITH
 * STUDENT - CURTIN UNIVERSITY (13954048)
 * 2015
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class CropActivity extends ActionBarActivity
{


    CustomDrawView customDrawView;
    Bitmap bitmap;
    FrameLayout layout_crop;
    String prefCountry;
    GlobalImageData data;
    private Context context;

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        context = this;

        Button leftButton = (Button)findViewById(R.id.leftButton);
        Button rightButton = (Button)findViewById(R.id.rightButton);
        //Set up onTouchListeners for the buttons
        leftButton.setOnTouchListener(listener);
        rightButton.setOnTouchListener(listener);

        SharedPreferences settings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        final String country = settings.getString("settings_country", "Australia");

        //Set up for dimming with popup.
        layout_crop = (FrameLayout) findViewById( R.id.mainmenu);
        //Dim level is set at 0. when a popup happens, the background will be dimmed. looks cool as hell.
        layout_crop.getForeground().setAlpha( 0);

        //Create PhotoDimentions
        final PhotoDimensionsForCountry photoDims = new PhotoDimensionsForCountry();
        data = ((GlobalImageData)getApplicationContext());

        int bitWidth = 0;
        int bitHeight = 0;

        try
        {
            //Get the bitmap from the saved file within the device
            bitmap = data.getImage(3);
            //Get Width and Height of the image's bitmap
            bitWidth = bitmap.getWidth();
            bitHeight = bitmap.getHeight();
            //Fetch ImageView from xml
            ImageView imgView = (ImageView)findViewById(R.id.imageView);
            //Set ImageView to the saved bitmap
            imgView.setImageBitmap(Bitmap.createScaledBitmap(bitmap, bitWidth/2, bitHeight/2, true));
        }
        catch(Exception E)
        {
            //Pop up to notify user something is wrong
            Toast.makeText(getApplicationContext(), "Failed to get image",
                    Toast.LENGTH_LONG).show();
            System.out.println("Failed to get image");
        }/**/

        //must be final to be used in the Listener
        final int inBitWidth = bitWidth;
        final int inBitHeight = bitHeight;

        //fetch the ImageView from XML
        final ImageView img = (ImageView)findViewById(R.id.imageView);
        final Context context = this;
        img.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                int relImgWidth;
                int relImgHeight;

                // gets called after layout has been done but before display.
                img.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                //Get relative to View bitmap width and height
                if(inBitWidth/inBitHeight > img.getWidth()/img.getHeight())
                {
                    relImgWidth = img.getWidth();
                    relImgHeight = relImgWidth * inBitHeight / inBitWidth;
                }
                else
                {
                    relImgHeight = img.getHeight();
                    relImgWidth = relImgHeight * inBitWidth / inBitHeight;
                }

                //Create a CustomDrawView
                customDrawView = new CustomDrawView(context, photoDims.getWidth(country), photoDims.getHeight(country), relImgWidth, relImgHeight, img.getWidth(), img.getHeight(), getGuidelines(photoDims.getOutline(country)));
                //add the CustomDrawView over the top of the image in the RelativeLayout
                RelativeLayout thisLayout = (RelativeLayout)findViewById(R.id.relative_layout);
                thisLayout.addView(customDrawView);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crop, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }

    View.OnTouchListener listener = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View v, MotionEvent event)
        {
            switch (v.getId())
            {
                /*
                LEFT BUTTON PRESSED
                 */
                case R.id.leftButton:
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:

                            break;

                        case MotionEvent.ACTION_UP :

                            break;
                    }
                    break;
                /*
                Next BUTTON PRESSED
                 */
                case R.id.rightButton:
                    switch (event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            findViewById(R.id.rightButton).setAlpha((float)0.5);
                            break;

                        case MotionEvent.ACTION_UP :
                            findViewById(R.id.rightButton).setAlpha((float)1.0);
                            //details of the crop box, x and y of crop box plus width and height
                            int[] details = customDrawView.getCropDetails();

                            int bitWidth = bitmap.getWidth();
                            int bitHeight = bitmap.getHeight();

                            for(int i = 0; i < details.length; i ++)
                            {
                                i++;
                            }

                            final Bitmap croppedBitmap = Bitmap.createBitmap(     bitmap,                         //Image being cropped
                            (int)((double)details[0] / (double)details[4] * (double)bitWidth),      //Scaled x coordinate of top left corner
                            (int)((double)details[1] / (double)details[5] * (double)bitHeight),     //Scaled y coordinate of top left corner
                            (int)((double)details[2] / (double)details[4] * (double)bitWidth),      //Scaled crop box width
                            (int)((double)details[3] / (double)details[5] * (double)bitHeight));    //Scaled crop box height

                            //Create the pop up
                            LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                            View popupView = layoutInflater.inflate(R.layout.preview_popup_dark_blue, null);
                            final PopupWindow popupWindow = new PopupWindow(popupView,
                                    AbsoluteLayout.LayoutParams.WRAP_CONTENT,
                                    AbsoluteLayout.LayoutParams.WRAP_CONTENT);
                            //Dim the background
                            layout_crop.getForeground().setAlpha(180);

                            // Get the activity's view
                            final View decorView = getWindow().getDecorView();
                            //The options to hide status bar
                            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;

                            final Button btnNext = (Button)popupView.findViewById(R.id.next);
                            final Button btnCancel = (Button)popupView.findViewById(R.id.cancel);

                            /*
                            Temporary Button to save the cropped photo
                            THIS WAS USED TO CREATE THE SAMPLE PHOTOS USED FOR COMPARISON.
                             /**
                            final EditText editText = (EditText)popupView.findViewById(R.id.tempEdit);
                            final Button btnTemp = (Button)popupView.findViewById(R.id.tempBtn);
                            btnTemp.setOnTouchListener(new Button.OnTouchListener()
                            {
                                @Override
                                public boolean onTouch(View v, MotionEvent theMotion)
                                {
                                    switch (theMotion.getAction())
                                    {
                                        case MotionEvent.ACTION_DOWN:
                                            btnTemp.setAlpha((float)0.5);
                                            break;
                                        case MotionEvent.ACTION_UP:
                                            popupWindow.dismiss();
                                            //Set dimness to zero again
                                            layout_crop.getForeground().setAlpha(0);

                                            saveImageToExternal(editText.getText().toString() + ".jpg", croppedBitmap);
                                            //String path = MediaStore.Images.Media.insertImage(getContentResolver(), croppedBitmap, editText.getText().toString(), " ");
                                            //System.out.println(path);

                                            //Show action and status bar again
                                            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                                            decorView.setSystemUiVisibility(uiOptions);
                                            break;
                                    }
                                    return true;
                                }
                            });
                            /*
                            End Temporaty Button
                             */

                            //What happens if cancel button is pressed
                            btnCancel.setOnTouchListener(new Button.OnTouchListener()
                            {
                                @Override
                                public boolean onTouch(View v, MotionEvent theMotion)
                                {
                                    switch (theMotion.getAction())
                                    {
                                        case MotionEvent.ACTION_DOWN:
                                            btnCancel.setAlpha((float)0.5);
                                            break;
                                        case MotionEvent.ACTION_UP:
                                            popupWindow.dismiss();
                                            //Set dimness to zero again
                                            layout_crop.getForeground().setAlpha(0);

                                            //Show action and status bar again
                                            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                                            decorView.setSystemUiVisibility(uiOptions);
                                            break;
                                    }
                                    return true;
                                }
                            });

                            //What happens if Next button is pressed.
                            btnNext.setOnTouchListener(new Button.OnTouchListener()
                            {
                                @Override
                                public boolean onTouch(View v, MotionEvent theMotion)
                                {
                                    switch (theMotion.getAction())
                                    {
                                        case MotionEvent.ACTION_DOWN:
                                            btnNext.setAlpha((float)0.5);
                                            break;
                                        case MotionEvent.ACTION_UP:
                                            data.updateImageFour(croppedBitmap);
                                            popupWindow.dismiss();

                                            //Show action and status bar again
                                            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                                            decorView.setSystemUiVisibility(uiOptions);

                                            promptForChecks();
                                            break;
                                    }
                                    return true;
                                }
                            });

                            //set popups image to the cropped image
                            ImageView popImgView = (ImageView)popupView.findViewById(R.id.popup_image);
                            popImgView.setImageBitmap(croppedBitmap);
                            //set pop ups text
                            TextView popTextView = (TextView)popupView.findViewById(R.id.popup_text);
                            popTextView.setText(" This will be your final image. ");


                            DisplayMetrics metrics = context.getResources().getDisplayMetrics();

                            //Show the Window
                            popupWindow.showAtLocation(popupView, Gravity.CENTER, -1, -1);
                            //popupWindow.setFocusable(true);
                            //popupWindow.update();

                            //Measure the height and width of the popupWindow
                            popupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                            //check if the popup is as big as the screen, including the status bar

                            if(popupView.getMeasuredHeight() > metrics.heightPixels - getStatusBarHeight())
                            {
                                //Hide the status bar
                                decorView.setSystemUiVisibility(uiOptions);
                                popupWindow.setHeight(metrics.heightPixels);
                                popupWindow.dismiss();
                                popupWindow.showAtLocation(popupView, Gravity.TOP, -1, -1);
                            }
                            break;
                    }
                    break;

                default:
                    break;
            }

            return false;
        }
    };

    private void promptForChecks()
    {
        //Create the pop up
        LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.to_distortion_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView,
                AbsoluteLayout.LayoutParams.WRAP_CONTENT,
                AbsoluteLayout.LayoutParams.WRAP_CONTENT);
        TextView popText = (TextView)popupView.findViewById(R.id.popText);
        popText.setText("Would you like to perform a\ncomparison check against some stock,\nstandard compliant photos?");

        final Button btnYes = (Button)popupView.findViewById(R.id.yes_button);
        final Button btnNo = (Button)popupView.findViewById(R.id.no_button);

        final Context context = this;
        //What happens if Yes button is pressed
        btnYes.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion) {
                switch (theMotion.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnYes.setAlpha((float) 0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        popupWindow.dismiss();
                        btnYes.setAlpha((float) 1.0);
                        layout_crop.getForeground().setAlpha(0);

                        Intent intent = new Intent(context, RequirementsCheckActivity.class);
                        startActivity(intent);
                        layout_crop.getForeground().setAlpha(0);
                        finish();
                        break;
                }
                return true;
            }
        });

        //What happens if Next button is pressed.
        btnNo.setOnTouchListener(new Button.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent theMotion) {
                switch (theMotion.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnNo.setAlpha((float) 0.5);
                        break;
                    case MotionEvent.ACTION_UP:
                        btnNo.setAlpha((float) 1.0);
                        popupWindow.dismiss();
                        layout_crop.getForeground().setAlpha(0);
                        Intent intent = new Intent(context, PrintReadyActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                }
                return true;
            }
        });

        //Show the Window
        popupWindow.showAtLocation(popupView, Gravity.CENTER, -1, -1);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }

    /*
     * Get Guidelines based on a preference country name
     */
    public Bitmap getGuidelines(String outlineImageName)
    {
        //Basically getting and returning the resource located at R.drawable/outlineImageName
        int resourceId = getResources().getIdentifier("drawable/" + outlineImageName, null, getPackageName());
        return ((BitmapDrawable)getResources().getDrawable(resourceId)).getBitmap();
    }

    /*
     * Returns the height of the status bar at the top of the adnroids display
     */
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void saveImageToExternal(String imgName, Bitmap bm)
    {
        //Create Path to save Image
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+"appFolder"); //Creates app specific folder
        path.mkdirs();
        File imageFile = new File(path, imgName+".png"); // Imagename.png
        try{
            FileOutputStream out = new FileOutputStream(imageFile);
            bm.compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
            out.flush();
            out.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(context, new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri)
                {
                }
            });
        }
        catch(Exception e)
        {
        }
    }
}
