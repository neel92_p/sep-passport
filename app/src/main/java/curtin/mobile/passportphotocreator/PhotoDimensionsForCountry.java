package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import java.util.ArrayList;

/**
 * Created by neel on 30/03/2015.
 */


//CountryDimensions holds the passport photo dimensions for every country.
    //The dimensions are stored in an array which is filled in the constructor.
//This class has no setter. The dimensions to be modified (or to create new ones) are to be
//modified (or added) in the constructor. This way the data of dimensions will not be lost when
//the application is closed and relaunched.

//All the values in dimensions are in mm.

//Test if getting back resources works.


/*
//THIS CODE IS USED TO ACCESS THE HEIGHT AND WIDTH OF THE COUNTRY THAT IS CURRENTLY SET AS
//THE PREFERRED COUNTRY IN HTE SETTINGS OF THE APPLICATION.

SharedPreferences settings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = settings.getString("settings_country", "Australia");
        CountryDimensions dimens = new CountryDimensions();
        double height = dimens.getHeight(country);
        double width = dimens.getWidth(country);
        String imageName = dimens.getOutline(country);

*/

public class PhotoDimensionsForCountry
{

    //This array list will be used to store dimensions.
    private ArrayList<Dimensions> dimensionsList;

    //This constructor create an array of dimensions that will hold the width x height for passport
    //photos for every country.
    public PhotoDimensionsForCountry()
    {
        dimensionsList = new ArrayList<Dimensions>();

        //The dimensions are added in here.
        Dimensions temp = new Dimensions();

        temp.country = "United States Of America";
        temp.height = 50;
        temp.width = 50;
        temp.outlineImageName = "american";
        temp.flagName = "united_states_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Mexico";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "mexico_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Canada";
        temp.height = 70;
        temp.width = 50;
        temp.outlineImageName = "canadian";
        temp.flagName = "canada_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "United Kingdom";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "united_kingdom_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Germany";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "germany_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "France";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "france_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Italy";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "italy_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Russia";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "russia_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Turkey";
        temp.height = 60;
        temp.width = 50;
        temp.outlineImageName = "turkey";
        temp.flagName = "turkey_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Netherlands";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "netherlands_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Australia";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "australia_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Japan";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "japan_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Singapore";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "singapore_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Korea";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "korea_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Spain";
        temp.height = 40;
        temp.width = 30;
        temp.outlineImageName = "spain";
        temp.flagName = "spain_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "China";
        temp.height = 48;
        temp.width = 33;
        temp.outlineImageName = "chinese";
        temp.flagName = "china_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "India";
        temp.height = 50;
        temp.width = 50;
        temp.outlineImageName = "american";
        temp.flagName = "india_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Brazil";
        temp.height = 45;
        temp.width = 35;
        temp.outlineImageName = "european";
        temp.flagName = "brazil_flag";
        dimensionsList.add(temp);

        temp = new Dimensions();
        temp.country = "Kenya";
        temp.height = 50;
        temp.width = 50;
        temp.outlineImageName = "american";
        temp.flagName = "kenya_flag";
        dimensionsList.add(temp);

    }

    /**
     * Returns the width of the photo in mm for that country
     * @param inCountry
     * @return width in mm, returns -1 if country not found.
     */
    public double getWidth(String inCountry)
    {
        double width = -1;

        for (Dimensions dimen: dimensionsList)
        {
            if (dimen.country.equals(inCountry))
            {
                width = dimen.width;
            }
        }

        return width;
    }

    /**
     * Returns the height of the photo in mm for that country.
     * @param inCountry
     * @return height in mm, returns -1 if country not found.
     */
    public double getHeight(String inCountry)
    {
        double height = -1;

        for (Dimensions dimen: dimensionsList)
        {
            if (dimen.country.equals(inCountry))
            {
                height = dimen.height;
            }
        }

        return height;
    }

    //Use the country name to get the country outline to display in the camera view.
    //Every country has its own rules and guideline and the name of this guidelines image
    public String getOutline(String inCountry)
    {
        String outline = null;

        for (Dimensions dimen: dimensionsList)
        {
            if (dimen.country.equals(inCountry))
            {
                outline = dimen.outlineImageName;
            }
        }

        return outline;
    }

    public String getFlag (String inCountry)
    {
        String flag = null;

        for (Dimensions dimen: dimensionsList)
        {
            if (dimen.country.equals(inCountry))
            {
                flag = dimen.flagName;
            }
        }

        return flag;
    }

    //This is a holder for the dimensions for every country.
    static class Dimensions
    {
        String country;
        double width;
        double height;
        String outlineImageName;
        String flagName;
    }

}


