package curtin.mobile.passportphotocreator;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


public class Startup_settings extends Activity implements AdapterView.OnItemSelectedListener {
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor sharedPrefEditor;
    Button doneButton;
    Spinner countrySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup_settings);

        sharedPref = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();

        doneButton = (Button) findViewById(R.id.startupScreen_doneButton);
        countrySpinner = (Spinner) findViewById(R.id.startupScreen_countrySpinner);

        //You cannot continue until you select a country.
        doneButton.setEnabled(true);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.pref_countries_values, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        countrySpinner.setAdapter(adapter);


        //Set this class as the the selectedListener to the country spinner.
        //This will allow the functions onItemSelected() and onNothingSelected()
        //to be used for the countrySpinner.
        countrySpinner.setOnItemSelectedListener(this);


        //Set the listener for the doneButton().
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Finish the activity when the back button is closed.
    @Override
    public void onBackPressed() {
        finish();
    }


    /*
        * These two functions, onItemSelected() and onNothingSelected(), are used to
        * implement what happens when a country is selected from the country spinner.
        * It also implements what happens when nothing is selected.
     */
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)

        String countrySelected = parent.getItemAtPosition(pos).toString();

        sharedPrefEditor.putString("settings_country", countrySelected);
        sharedPrefEditor.commit();
        sharedPrefEditor.putBoolean("isStartupScreenAlreadyDisplayed", true);
        sharedPrefEditor.commit();

        //After the country is selected, enable the done button.
        doneButton.setEnabled(true);


    }

    //If nothing is selected, simple don't enable the done button.
    public void onNothingSelected(AdapterView<?> parent) {

        // Another interface callback
    }



}
