package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class ProjectPreview extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        //Remove the action bar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        //Get any data passed in via putExtra.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String imagePath = null;
        if (extras != null)
        {
            imagePath = extras.getString("IMAGE_PATH");
        }

        retrieveImage(imagePath);


        //This is the listener for the button used to select another image if the user doesn't like the one currently chosen.
        ((Button) findViewById(R.id.retakeButton))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        //Finish preview and go back to the projects view.
                        finish();
                    }
                });

        //This is the listener for the yes button that marks the user is happy with the current image.

        ((Button) findViewById(R.id.yesButton))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            //Start a new intent and move to next activity.
                            //Make sure the path selectedImagePath is passed as an extra using putExtra();

                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_project_preview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }




    /*
    * Reason for this onBackPressed is to override what the system does when the back button on the
    * device is clicked. In this case, nothing. This means that i've disabled the back button on this
    * activity so that the user is able to choose the image from the gallery again.
     */
    /*
    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
    }

    /**
     * This function is called to create a preview of the image and put it in the
     * preview box.
     * @param path - path of the image selected.
     */
    public void retrieveImage (String path)
    {

        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        if (path != null) {
            ImageManipulation imgManipulation = new ImageManipulation();
            //The 300 is the width required. The 400 is the height of the image after the
            //resolution has been reduced.
            //Bitmap lowerResolutionImage = imgManipulation.lessResolution(path, 480, 640);
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            //Rotate the picture to make it portrait.
            bitmap = imgManipulation.rotateImage(bitmap, 90);
            //Use the lowerResolutionImage is displayed on the imageView of this activity.

            //Set the image on the ImageView.
            imageView.setImageBitmap(bitmap);
        }

    }


}

