package curtin.mobile.passportphotocreator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;


public class ImageEditingActivity extends ActionBarActivity {


    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editing_image);

        //Obtaining the image via putExtra and getExtra
        final GlobalImageData data = ((GlobalImageData)getApplicationContext());
        final ImageView imageView = (ImageView) findViewById(R.id.imagePreview);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        Bitmap image = null;
        String imagePath = null;

        if(extras != null)
        {
            imagePath = (extras.getString("IMAGE"));
        }
        data.setImage(imagePath);
        final Bitmap lowResImage = Bitmap.createScaledBitmap(data.getImage(1),
                data.getImage(1).getWidth()/3, data.getImage(1).getHeight()/3, true);
        //Retrieving and displaying the image
        if (imagePath != null)
        {
            File imageFile = new File(imagePath);
            image = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            imageView.setImageBitmap(lowResImage);
        }

        //Initialising the seekBars
        final SeekBar seekBarBright = (SeekBar)findViewById(R.id.seekBarBright);
        seekBarBright.setMax(510);
        seekBarBright.setProgress(255);

        final SeekBar seekBarContrast = (SeekBar)findViewById(R.id.seekBarContrast);
        seekBarContrast.setMax(100);
        seekBarContrast.setProgress(10);

        final TextView brightValue = (TextView)findViewById(R.id.brightnessValue);
        final TextView contrastValue = (TextView)findViewById(R.id.contrastValue);

        //This is the listener used for scrolling the contrast seekbar. It changes the
        //contrast of the low res image when moved
        seekBarContrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                contrastValue.setText(String.valueOf((float)(seekBar.getProgress())/10));
                imageView.setImageBitmap(editBrightCon(lowResImage,
                        (float)(progress)/10, (float)(seekBarBright.getProgress()-255)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });

        //This is the listener used for scrolling the brightness seekbar. It changes the
        //brightness of the low res image when moved
        seekBarBright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, final int progress, boolean fromUser)
            {
                double value = ((double)seekBar.getProgress() / 255.0) * 100.0;
                brightValue.setText(String.valueOf((int)value - 100));
                imageView.setImageBitmap(editBrightCon(lowResImage,
                        (((float)seekBarContrast.getProgress() / 10)), (float)(progress - 255)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {

            }
        });

        //This is the listener for the button used to reset the brightness and contrast back to
        // standard configuration
        (findViewById(R.id.resetButton))
                .setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        seekBarBright.setProgress(255);
                        seekBarContrast.setProgress(10);
                        imageView.setImageBitmap(editBrightCon(
                                lowResImage,
                                (float)((seekBarContrast.getProgress()/10)),
                                (float)(seekBarBright.getProgress() - 255)));
                    }
                });

        final Context context = this;
        //This is the listener for the button used to go to the next screen, presumably cropping
        (findViewById(R.id.nextButton))
                .setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        //Start a new intent and move to next activity.
                        //Make sure the edited image is saved and path is sent using putExtra
                        if(data.getImage(1) != null)
                        {
                            Intent intent = new Intent(context, RotationActivity.class);
                            data.updateImageTwo(editBrightCon(data.getImage(1),
                                    (float) ((seekBarContrast.getProgress() / 10)),
                                    (float) (seekBarBright.getProgress() - 255)));
                            startActivity(intent);
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_image_editing, menu);

        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
        String country = sharedSettings.getString("settings_country", "default");
        PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
        String flagName = dimens.getFlag(country);

        if (flagName == null)
        {
            flagName = "australia_flag.png";
        }
        int resourceId = getResources().getIdentifier(flagName, "drawable",
                getPackageName());

        menu.getItem(0).setIcon(resourceId);


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (menu != null)
        {
            SharedPreferences sharedSettings = getSharedPreferences("MySharedPreferences", MODE_PRIVATE);
            String country = sharedSettings.getString("settings_country", "default");
            PhotoDimensionsForCountry dimens = new PhotoDimensionsForCountry();
            String flagName = dimens.getFlag(country);

            if (flagName == null)
            {
                flagName = "australia_flag.png";
            }
            int resourceId = getResources().getIdentifier(flagName, "drawable",
                    getPackageName());
            menu.getItem(0).setIcon(resourceId);
        }

    }

    //Function from stack overflow
    //http://stackoverflow.com/questions/27679334/android-changing-image-contrast-and-brightness-upon-scrolling-seekbar
    public Bitmap editBrightCon(Bitmap bmp, float contrast, float brightness)
    {
        if(((contrast < 0.99) || (contrast > 1.01)) || ((brightness < -0.01) || (brightness > 0.01)))
        {
            ColorMatrix cm = new ColorMatrix(new float[]
                    {
                            contrast, 0, 0, 0, brightness,
                            0, contrast, 0, 0, brightness,
                            0, 0, contrast, 0, brightness,
                            0, 0, 0, 1, 0
                    });
            Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

            Canvas canvas = new Canvas(ret);

            Paint paint = new Paint();
            paint.setColorFilter(new ColorMatrixColorFilter(cm));
            canvas.drawBitmap(bmp, 0, 0, paint);
            System.out.println("Testing brightness function - CHANGE");
            return ret;
        }
        else
        {
            System.out.println("Testing brightness function - no change");
            return bmp;
        }
    }
}
