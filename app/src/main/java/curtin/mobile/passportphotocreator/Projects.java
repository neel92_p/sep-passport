package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.app.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.Gallery;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class Projects extends Activity {

    private GridView gridView;
    private GridViewAdapter customGridAdapter;

    private static int lastPictureLoaded = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);

        gridView = (GridView) findViewById(R.id.gridView);
        //gridView.setAdapter(new GridViewAdapter(this, R.layout.row_grid, new ArrayList()));
        //ArrayList data = getData();


        //ArrayList data holds the Bitmap objects that will be displayed by the GridView object.
        ArrayList data = new ArrayList();
        //data.add(new ImageItem(null, "test"));
        customGridAdapter = new GridViewAdapter(this, R.layout.row_grid, data);
        gridView.setAdapter(customGridAdapter);

        //new FetchImagesAsync().execute(customGridAdapter);

        /////Runs on a separate thread.
        /*
        new Thread()
        {
            public void run()
            {
                String directoryLocation = "/sdcard/PassportPhotoApps/Images";

                //Open the directory where all the pictures are located.
                File file = new File(directoryLocation);
                //Get all files in the directory
                File[] list = file.listFiles();
                int count = 1;
                //For every file in the list, open if it is a .png and insert in the array imageItems.
                for (File f: list)
                {

                    final String name = f.getName();
                    ImageManipulation imgManipulation = new ImageManipulation();
                    //Only read the images ending with a .png
                    if (name.endsWith(".png")) {

                        String absolutePath = f.getAbsolutePath();


                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = imgManipulation.calculateInSampleSize(options, 50, 50);
                        Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, options);

                        if (bitmap != null) {
                            //Rotate the image so that it is displayed as portrait
                            bitmap = imgManipulation.rotateImage(bitmap, 90);
                            final Bitmap temp = bitmap;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    customGridAdapter.add(new ImageItem(temp, name));
                                }
                            });

                        }

                    }


                }
            }
        }.start();
        /////END OF TESTING CODE
        */



        //The PhotoDecoder object is used to process of PhotoTask's run() function on separate threads.
        PhotoDecoder decoder = new PhotoDecoder();
        String directoryLocation = getString(R.string.directory);
        File dir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + directoryLocation);
        File [] list = dir.listFiles();

        //Load first 20 picture when the activity starts.
        loadNextTwentyPictures(lastPictureLoaded);

//        ArrayList<PhotoTask> task = new ArrayList<>();
        //For every file in the storage directory, create a PhotoTask object, the object that runs
        //on a separate thread.
//        for (File f: list)
//        {
//            task.add(new PhotoTask(f.getAbsolutePath()));
//        }
//
//        try {
//            //Add the files to loads to the photo decoder handler that will load pictures using
//            //multi-threading.
//            decoder.addTasks(task);
//            //Get results.
//            ArrayList<PhotoTask> tasks = decoder.getResult();
//
//            //Add all these images to the gridViewAdapter source ArrayList "data".
//            for (PhotoTask currentTask : tasks)
//            {
//                customGridAdapter.add(new ImageItem(currentTask.getBitmap(), currentTask.getBitmapName()));
//            }
//        }
//        catch (Exception e)
//        {
//            Log.i("PROJECTS.JAVA ERROR", "Exception=" + e.getMessage());
//        }

        //This is a new setOnItemClickListener for gridView.
        gridView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id)
            {
                ImageItem item =(ImageItem) gridView.getAdapter().getItem(position);
                String imageName = item.getTitle();
                String absolutePath = Environment.getExternalStorageDirectory().getPath() + File.separator + getString(R.string.directory) + File.separator + imageName;

                startPreviewActivity(absolutePath);
            }
        });

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //If the user has performed a fling (used finger to scroll and lifted their picture up)
                //or if the user has scrolled and their finger is still on the screen, then load more pictures.
                if (scrollState == 0 || scrollState == 2)
                {
                    loadNextTwentyPictures(lastPictureLoaded);
                }

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

    }

    private void startPreviewActivity(String inAbsolutePath)
    {
        //Param imagePath is the path to the image selected by the user,
        //or the path of the photo recently taken.
        //Create an intent to start the application.
        Intent intent = new Intent (this, ProjectPreview.class);

        //Set extra information via put extra. Send the path to the image (selectedImagePath).
        if (inAbsolutePath != null)
        {
            intent.putExtra("IMAGE_PATH", inAbsolutePath);
            startActivity(intent);
        }
    }

/*
    private void fetchImagesForProjects ()
    {
        //customGridAdapter = new GridViewAdapter(this, R.layout.row_grid, getData());
        try {
            customGridAdapter = new FetchImagesAsync().execute(this).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        gridView.setAdapter(customGridAdapter);
    }
    */

    /*
    private ArrayList getData() {

        //This is the directory location of the application storage for images regarding this project.
        String directoryLocation = getString(R.string.directory);
        ArrayList imageItems = new ArrayList();



        /*
        final ArrayList imageItems = new ArrayList();
        //retrieve String drawable array

        //Get the images from the memory and insert them in the array.

        //Get the location of the directory where all captured images are stored.
        String directoryLocation = getString(R.string.directory);
        File file = new File(directoryLocation);
        //Get all files in the directory
        File[] list = file.listFiles();
        int count = 1;
        //For every file in the list, open if it is a .png and insert in the array imageItems.
        for (File f: list)
        {

            String name = f.getName();
            ImageManipulation imgManipulation = new ImageManipulation();
            //Only read the images ending with a .png
            if (name.endsWith(".png")) {

                String absolutePath = f.getAbsolutePath();


                BitmapFactory.Options options = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(absolutePath, options);

                if (bitmap != null) {
                    //Rotate the image so that it is displayed as portrait
                    bitmap = imgManipulation.rotateImage(bitmap, 90);


                    imageItems.add(new ImageItem(bitmap, name));
                }

            }


        }

        //Return the list.
        return imageItems;

        */



    /*
        //TRIED TO MULTITHREADED FETCHING.
        directoryLocation = getString(R.string.directory);
        PhotoDecodeRunnable runnable = new PhotoDecodeRunnable();
        imageItems = runnable.readFilesFromDirectory(directoryLocation);
        return imageItems;


    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * This function overrides what the back button does.
     * It will close the current activity when the back button is clicked.
     */
    @Override
    public void onBackPressed()
    {
        lastPictureLoaded = 0;
        finish();
    }





    /**
     * Load Next 20 pictures in the gridView. This way, if there are about 500 pictures on the device
     *not all are loaded at once. This function will be used to load pictures as the user scrolls.
     * @param index is the index of the last image loaded.
     */
    public void loadNextTwentyPictures(int index)
    {

        try {

            String directoryLocation = getString(R.string.directory);
            File dir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + directoryLocation);
            File [] list = dir.listFiles();
            ArrayList<PhotoTask> task = new ArrayList<PhotoTask>();


            //If more than 20 files are left, then load only 20.
            if (((list.length-1)-index) >= 20)
            {
                int ii;
                for (ii = index; ii <= index+20 ; ii++)
                {
                    File f = list[ii];
                    task.add(new PhotoTask(f.getAbsolutePath()));
                }
                //Update the last picture loaded index.
                lastPictureLoaded = ii;

            }
            else //if less than 20, load all the ones left.
            {
                int ii;
                for (ii=index; ii < list.length; ii++)
                {
                    File f = list[ii];
                    task.add(new PhotoTask(f.getAbsolutePath()));
                }
                //update the last picture loaded index.
                lastPictureLoaded = ii;
            }

            //If there are more files, then insert them in the grid view.
            if (task.size() > 0)
            {
                PhotoDecoder decoder = new PhotoDecoder();
                //Add the files to loads to the photo decoder handler that will load pictures using
                //multi-threading.
                decoder.addTasks(task);
                //Get results.
                ArrayList<PhotoTask> tasks = decoder.getResult();

                //Add all these images to the gridViewAdapter source ArrayList "data".
                for (PhotoTask currentTask : tasks)
                {
                    customGridAdapter.add(new ImageItem(currentTask.getBitmap(), currentTask.getBitmapName()));
                }
            }
        }
        catch (Exception e)
        {
            Log.i("PROJECTS.JAVA ERROR", "Exception=" + e.getMessage());
        }
    }
}
