package curtin.mobile.passportphotocreator;

/****
 * CREATOR: NEEL ASHOKBHAI PATEL
 * STUDENT - CURTIN UNIVERSITY (14895876)
 * 2015
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

/**
 * Created by neel on 3/04/2015.
 */


/**
 * This class is used to create objects that implement Runnable class.
 * Every object is capable of decoding an image on an independent thread.
 * It saves the image decoded as a private field, that needs to be accessed using the getter.
 */
public class PhotoTask implements Runnable{

    private Bitmap image = null;
    private String filename = null;
    private String absolutePath;

    /**
     * Constructor
     * @param path of the image to decode.
     */
    public PhotoTask(String path)
    {
        File file = new File(path);
        this.filename = file.getName();
        this.absolutePath = file.getAbsolutePath();

    }

    @Override
    public void run()
    {
        try
        {
            if (filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg") ) {

                ImageManipulation imgManipulation = new ImageManipulation();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                //options.inSampleSize = imgManipulation.calculateInSampleSize(options, 100, 100);
                image = BitmapFactory.decodeFile(this.absolutePath, options);

                if (image != null) {
                    //Rotate the image so that it is displayed as portrait
                    image = imgManipulation.rotateImage(image, 90);
                }

            }
        }
        catch (Exception e)
        {
            Log.i("PHOTO_DECODING_AT_PHOTOTASK", e.getMessage());
        }
    }

    /**
     * Gets the bitmap created.
     * @return bitmap that the runnable creates
     */
    public Bitmap getBitmap()
    {
        return this.image;
    }

    public String getBitmapName()
    {
        return this.filename;
    }

}

